package action;

import net.ihe.gazelle.simulator.svs.action.TransactionInstanceDisplay;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.mock;
import static org.testng.Assert.assertEquals;

public class TransactionInstanceDisplayTest {
    private static final String tosLink = "https://test.net";

    @Test
    public void getCguUrlTest_ok(){
        TransactionInstanceDisplay applicationManager = mock(TransactionInstanceDisplay.class);
        Mockito.when(applicationManager.getTosLink()).thenReturn(tosLink);
        assertEquals(tosLink,"https://test.net");
    }
    @Test
    public void getCguUrlTest_ko(){
        TransactionInstanceDisplay applicationManager = mock(TransactionInstanceDisplay.class);
        Mockito.when(applicationManager.getTosLink()).thenReturn(tosLink);
        assertFalse(tosLink.equals("this is test string"));
    }
}