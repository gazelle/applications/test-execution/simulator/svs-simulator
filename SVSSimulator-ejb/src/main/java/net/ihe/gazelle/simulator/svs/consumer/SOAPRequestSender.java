package net.ihe.gazelle.simulator.svs.consumer;

import net.ihe.gazelle.simulator.common.ihewsinit.SoapWebServiceClient;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.message.model.EStandard;
import org.w3c.dom.Element;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPMessage;

/**
 * Implementation of {@link SoapWebServiceClient}, only used for message sending.
 */
public class SOAPRequestSender extends SoapWebServiceClient{

    /**
     * {@inheritDoc}
     */
    @Override
    protected Element getAssertionFromSTS(String s, String s1) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void appendAssertionToSoapHeader(SOAPMessage soapMessage, Element element) {
        //Empty, no assertion will be used in this tool.
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Domain getDomainForTransactionInstance() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getIssuerName() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected QName getServiceQName() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected QName getPortQName() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Actor getSimulatedActor() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Transaction getSimulatedTransaction() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Actor getSutActor() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected EStandard getStandard() {
        return null;
    }
}
