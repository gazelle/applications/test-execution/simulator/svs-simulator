package net.ihe.gazelle.simulator.svs.model;

import org.jboss.seam.annotations.Name;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * Tool Usage is use to store call of the RetrieveValueSetForSimulator
 * 
 * @author mtual
 */
@Entity
@Name("toolUsage")
@Table(name = "svs_tool_usage", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = { "id",
		"callerIp" }))
@SequenceGenerator(name = "svs_toolU_sequence", sequenceName = "svs_toolU_id_seq", allocationSize = 1)
public class ToolUsage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8075715244224606474L;

	@Id
	@Column(name = "id", nullable = false, unique = true)
	@GeneratedValue(generator = "svs_toolU_sequence", strategy = GenerationType.SEQUENCE)
	@NotNull
	private Integer id;

	@Column(name = "callerIp")
	private String callerIp;

	@Column(name = "lastCall")
	private Date lastCall;

	@Column(name = "nbOfCalls")
	private Integer nbOfCalls;

	public ToolUsage() {
		this.lastCall = new Date();
		this.nbOfCalls = 1;
	}

	public ToolUsage(String callerIp) {
		this.callerIp = callerIp;
		this.lastCall = new Date();
		this.nbOfCalls = 1;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCallerIp() {
		return callerIp;
	}

	public void setCallerIp(String callerIp) {
		this.callerIp = callerIp;
	}

	public Date getLastCall() {
		return lastCall;
	}

	public void setLastCall(Date lastCall) {
		this.lastCall = lastCall;
	}

	public Integer getNbOfCalls() {
		return nbOfCalls;
	}

	public void setNbOfCalls(Integer nbOfCalls) {
		this.nbOfCalls = nbOfCalls;
	}

}
