package net.ihe.gazelle.simulator.svs.dao;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.datatypes.CE;
import net.ihe.gazelle.datatypes.CEQuery;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.svs.*;
import net.ihe.gazelle.svs.svs.ValueSetTypeValidator;
import net.ihe.gazelle.validation.Notification;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Name("describedValueSetDAO")
public class DescribedValueSetDAOImpl implements DescribedValueSetDAO {

    private EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");

    private Filter<DescribedValueSet> filterForGroup;

    /**
     * Reset filter in valueSetBrowser
     */

    public Filter<DescribedValueSet> getFilterForGroup(GroupType group) {
        if (filterForGroup == null) {
            filterForGroup = new Filter<DescribedValueSet>(getHQLCriteriaForFilterForGroup(group));
        }
        return filterForGroup;
    }

    public HQLCriterionsForFilter<DescribedValueSet> getHQLCriteriaForFilterForGroup(final GroupType group) {
        final DescribedValueSetQuery query = new DescribedValueSetQuery();
        HQLCriterionsForFilter<DescribedValueSet> criteria = query.getHQLCriterionsForFilter();

        criteria.addQueryModifier(new QueryModifier<DescribedValueSet>() {
            @Override
            public void modifyQuery(HQLQueryBuilder<DescribedValueSet> hqlQueryBuilder, Map<String, Object> map) {
                DescribedValueSetQuery q = new DescribedValueSetQuery(hqlQueryBuilder);
                if (group != null) {
                    hqlQueryBuilder.addRestriction(HQLRestrictions.or(
                            q.group().iD().neqRestriction(group.iD),
                            q.group().isNotEmptyRestriction()));
                }
            }

        });

        return criteria;
    }

    public FilterDataModel<DescribedValueSet> getAllValueSetsForSelectedGroup(GroupType group) {

        return new FilterDataModel<DescribedValueSet>(getFilterForGroup(group)) {
            @Override
            protected Object getId(DescribedValueSet describedValueSet) {
                return describedValueSet.getId();
            }

            @Override
            public DescribedValueSet getRowData() {
                if (this.getDataItem() != null) {
                    HQLQueryBuilder queryBuilder;
                    if (this.getEntityManager() != null) {
                        queryBuilder = new HQLQueryBuilder(this.getEntityManager(), this.getDataClass());
                    } else {
                        queryBuilder = new HQLQueryBuilder(this.getDataClass());
                    }
                    queryBuilder.addEq("id", this.getDataItem());
                    return (DescribedValueSet) queryBuilder.getUniqueResult();
                }
                return null;
            }
        };
    }

    /**
     * Retrieve a value set id from URL and return that value set
     *
     * @param urlParams URL containing the id of a value set
     * @return DescribedValueSet Value set retrieved from DB (with the id in URL)
     */
    public DescribedValueSet getValueSetFromUrl(Map<String, String> urlParams) {
        HQLQueryBuilder<DescribedValueSet> queryBuilder = new HQLQueryBuilder<DescribedValueSet>(
                DescribedValueSet.class);
        queryBuilder.addEq("id", urlParams.get("id"));
        return queryBuilder.getUniqueResult();
    }

    /**
     * Save the Value Set in database
     *
     * @param valueSet Value set to retrieve from DB
     * @return DescribedValueSet Value set retrieved from DB
     */
    public DescribedValueSet getValueSetfromDB(DescribedValueSet valueSet) {
        HQLQueryBuilder<DescribedValueSet> queryBuilder = new HQLQueryBuilder<DescribedValueSet>(
                DescribedValueSet.class);
        queryBuilder.addEq("id", valueSet.getId());
        return queryBuilder.getUniqueResult();
    }

    /**
     * Save the edited Value Set in database
     *
     * @param valueSet Edited value set to save in DB
     */
    public void saveEditValueSet(DescribedValueSet valueSet) {
        HQLQueryBuilder<DescribedValueSet> queryBuilder = new HQLQueryBuilder<DescribedValueSet>(
                DescribedValueSet.class);
        queryBuilder.addEq("id", valueSet.getId());
        DescribedValueSet exist = queryBuilder.getUniqueResult();

        // If the VS doesn't exist in the database we just merge him
        if (exist == null) {
            // We passed the Value Set to the validator before insert it in database
            List<Notification> diagnostic = new ArrayList<Notification>();
            ValueSetTypeValidator._validateValueSetType(valueSet, "New Value Set", diagnostic);

            List<Notification> errors = new ArrayList<Notification>();
            for (Notification d : diagnostic) {
                if (d.getDescription().contains("ERROR")) {
                    errors.add(d);
                }
            }

            // If the validators don't return errors
            if (errors.size() <= 0) {
                valueSet = entityManager.merge(valueSet);
                entityManager.flush();
                FacesMessages.instance().add("The Value Set has been successfully saved.");
            } else {
                FacesMessages.instance().add("You have some errors in your Value Set Object.");
                for (Notification not : errors) {
                    FacesMessages.instance().add(not.getDescription());
                }
            }
        } else // In the other case we need to set all the id for ConceptList and Concept first, then save in database
        {
            // Set des ID_DB pour chaque VS,ConceptList,Concept avant de merge
            valueSet.setIdDB(exist.getIdDB());

            for (ConceptListType clt : valueSet.getConceptList()) {
                clt.setValueSet(valueSet);
                for (CE c : clt.getConcept()) {
                    c.setConceptList(clt);
                }
            }

            // Save BDD
            valueSet = entityManager.merge(valueSet);
            entityManager.flush();
            FacesMessages.instance().add("The Value Set has been successfully saved.");

            reloadValueSet(valueSet);
        }
    }

    /**
     * Save the edited Value Set in database and redirect
     *
     * @param valueSet Edited value set to save in DB
     * @return String Part of the URL in order to redirect to edition page
     */
    public String saveEditValueSetAndRedirect(DescribedValueSet valueSet) {
        HQLQueryBuilder<DescribedValueSet> queryBuilder = new HQLQueryBuilder<DescribedValueSet>(
                DescribedValueSet.class);
        queryBuilder.addEq("id", valueSet.getId());
        DescribedValueSet exist = queryBuilder.getUniqueResult();

        // If the VS doesn't exist in the database we just merge him
        if (exist == null) {
            // We passed the Value Set to the validator before insert it in database
            List<Notification> diagnostic = new ArrayList<Notification>();
            ValueSetTypeValidator._validateValueSetType(valueSet, "New Value Set", diagnostic);

            List<Notification> errors = new ArrayList<Notification>();
            for (Notification d : diagnostic) {
                if (d.getDescription().contains("ERROR")) {
                    errors.add(d);
                }
            }

            // If the validators don't return errors
            if (errors.size() <= 0) {
                valueSet = entityManager.merge(valueSet);
                entityManager.flush();
                FacesMessages.instance().add("The Value Set has been successfully saved.");
                return "/browser/editValueSet.seam?id=" + valueSet.getId();
            } else {
                FacesMessages.instance().add("You have some errors in your Value Set Object.");
                for (Notification not : errors) {
                    FacesMessages.instance().add(not.getDescription());
                }
            }
            return null;
        } else // In the other case we need to set all the id for ConceptList and Concept first, then save in database
        {
            // Set des ID_DB pour chaque VS,ConceptList,Concept avant de merge
            valueSet.setIdDB(exist.getIdDB());

            for (ConceptListType clt : valueSet.getConceptList()) {
                clt.setValueSet(valueSet);
                for (CE c : clt.getConcept()) {
                    c.setConceptList(clt);
                }
            }

            // Save BDD
            valueSet = entityManager.merge(valueSet);
            entityManager.flush();

            reloadValueSet(valueSet);
            return "/browser/editValueSet.seam?id=" + valueSet.getId();

        }
    }

    /**
     * Save the Value Set in database
     *
     * @param valueSet Value set to save
     */
    public void saveValueSet(DescribedValueSet valueSet) {
        entityManager.merge(valueSet);
        entityManager.flush();
    }

    /**
     * Save the Value Set in database and redirect to edition page
     *
     * @param valueSet Value set to save
     * @return Return the redirection String (editPage)
     */
    public String saveValueSetAndRedirect(DescribedValueSet valueSet) {
        HQLQueryBuilder<DescribedValueSet> queryBuilder = new HQLQueryBuilder<DescribedValueSet>(
                DescribedValueSet.class);
        queryBuilder.addEq("id", valueSet.getId());
        DescribedValueSet exist = queryBuilder.getUniqueResult();

        // If the VS doesn't exist in the database we just merge him
        if (exist == null) {
            // We passed the Value Set to the validator before insert it in database
            List<Notification> diagnostic = new ArrayList<Notification>();
            ValueSetTypeValidator._validateValueSetType(valueSet, "New Value Set", diagnostic);

            List<Notification> errors = new ArrayList<Notification>();
            for (Notification d : diagnostic) {
                if (d.getDescription().contains("ERROR")) {
                    errors.add(d);
                }
            }

            // If the validators don't return errors
            if (errors.size() <= 0) {
                valueSet = entityManager.merge(valueSet);
                entityManager.flush();
                FacesMessages.instance().add("The Value Set has successfully been saved.");
                return "/browser/editValueSet.seam?id=" + valueSet.getId();
            } else {
                FacesMessages.instance().add("You have some errors in your Value Set Object.");
                for (Notification not : errors) {
                    FacesMessages.instance().add(not.getDescription());
                }
            }
        } else // In the other case we need to set all the id for ConceptList and Concept first, then save in database
        {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "A ValueSet with the oid " + valueSet.getId() + " already exists.");

        }
        return null;
    }

    /**
     * Delete a Value Set
     *
     * @param valueSet The ValueSet to delete
     */
    public void deleteValueSet(DescribedValueSet valueSet) {
        HQLQueryBuilder<DescribedValueSet> queryBuilder = new HQLQueryBuilder<DescribedValueSet>(
                DescribedValueSet.class);
        queryBuilder.addEq("id", valueSet.getId());
        DescribedValueSet removeVS = queryBuilder.getUniqueResult();

        EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
        entityManager.remove(removeVS);
        entityManager.flush();
        valueSet = new DescribedValueSet();
        FacesMessages.instance().add("The Value Set has been deleted.");
    }

    /**
     * Delete a concept from all conceptList of the specific Value Set
     *
     * @param valueSet Specific value set to which concepts have to be deleted
     * @param ce The Concept to delete
     */
    public void deleteConceptFromValueSet(DescribedValueSet valueSet, CE ce) {
        // Get all the conceptList of a valueSet
        List<ConceptListType> listConceptListToModify = new ArrayList<ConceptListType>();
        CE codeDBTodelete;
        for (ConceptListType conceptList : valueSet.getConceptList()) {
            ConceptListType conceptListDB = entityManager.find(ConceptListType.class, conceptList.getIdDB());
            CEQuery ceQuery = new CEQuery();
            ceQuery.code().eq(ce.getCode());
            ceQuery.conceptList().idDB().eq(conceptListDB.getIdDB());
            codeDBTodelete = ceQuery.getUniqueResult();
            conceptListDB.removeConcept(codeDBTodelete);
            entityManager.remove(codeDBTodelete);
            listConceptListToModify.add(conceptListDB);
        }
        valueSet.setConceptList(listConceptListToModify);
        valueSet = entityManager.merge(valueSet);
        entityManager.flush();
    }

    /**
     * Delete a concept from a conceptList and persist Value Set at the end
     *
     * @param valueSet Specific value set to which concepts have to be deleted
     * @param conceptListDB Concept List which will have a concept deleted
     * @param ce The Concept to delete
     */
    public void deleteSpecificConceptFromValueSet(DescribedValueSet valueSet, ConceptListType conceptListDB, CE ce) {
        CEQuery ceQuery = new CEQuery();
        ceQuery.code().eq(ce.getCode());
        ceQuery.conceptList().idDB().eq(conceptListDB.getIdDB());
        CE codeDBTodelete = ceQuery.getUniqueResult();
        conceptListDB.removeConcept(codeDBTodelete);
        entityManager.remove(codeDBTodelete);
        List<ConceptListType> listConceptListToModify = valueSet.getConceptList();
        listConceptListToModify.remove(conceptListDB);
        listConceptListToModify.add(conceptListDB);
        valueSet.setConceptList(listConceptListToModify);
        valueSet = entityManager.merge(valueSet);
        entityManager.flush();
    }

    /**
     * Allow to know if a value set is in a group
     *
     * @param valueSet The Value Set to test
     * @return Boolean (true: has group / false: doesn't have group)
     */
    public Boolean getBoolGroupListEmpty(DescribedValueSet valueSet) {
        // Get the Value Set with getGroup loaded
        DescribedValueSet vs = entityManager.find(DescribedValueSet.class, valueSet.getIdDB());
        // Test size list group
        if (vs.getGroup() == null || vs.getGroup().isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Add imported value set (imported from multiple value set response)
     *
     * @param vs Value set imported from the response
     */
    public void addDescribedValueSetFromMultipleValueSetResponse(DescribedValueSet vs) {
        Boolean firstClt = true;
        for (ConceptListType clt : vs.getConceptList()) {
            if (firstClt) {
                clt.setMainList(true);
                firstClt = false;
            }
            clt.setValueSet(vs);
            for (CE c : clt.getConcept()) {
                c.setConceptList(clt);
                if (c.codeSystemName == null) {
                    c.codeSystemName = "";
                }
                if (c.codeSystemVersion == null) {
                    c.codeSystemVersion = "";
                }
                if (c.codeSystem == null) {
                    c.codeSystem = "";
                }
            }
        }

        // Prepare the object
        if (vs.getId() == null) {
            vs.setId(vs.getId());
        }
        if (vs.getStatus() == null) {
            vs.setStatus("Active");
        }
        if (vs.getSourceURI() == null) {
            vs.setSourceURI("");
        }
        if (vs.getSource() == null) {
            vs.setSource("");
        }
        if (vs.getPurpose() == null) {
            vs.setPurpose("");
        }
        if (vs.getDefinition() == null) {
            vs.setDefinition("");
        }
        if (vs.getBinding() == null) {
            vs.setBinding(BindingType.STATIC);
        }
        if (vs.getType() == null) {
            vs.setType(TypeType.INTENSIONAL);
        }
        if (vs.getEffectiveDate() != null) {
            vs.setEffectiveDate(vs.getEffectiveDate());
        }
        if (vs.getExpirationDate() != null) {
            vs.setExpirationDate(vs.getExpirationDate());
        }
        if (vs.getCreationDate() != null) {
            vs.setCreationDate(vs.getCreationDate());
        }
        if (vs.getRevisionDate() != null) {
            vs.setRevisionDate(vs.getRevisionDate());
        }

        entityManager.persist(vs);
        entityManager.merge(vs);
        entityManager.flush();
    }

    /**
     * Manage imported value set attributes (imported from single value set response)
     *
     * @param vs Value set imported from the response
     */
    public void treatDescribedValueSetImportedFromRetrieveValueSetResponse(DescribedValueSet vs) {
        HQLQueryBuilder<DescribedValueSet> queryBuilder = new HQLQueryBuilder<DescribedValueSet>(
                DescribedValueSet.class);
        queryBuilder.addEq("id", vs.getId());
        DescribedValueSet exist = queryBuilder.getUniqueResult();
        if (exist == null) {
            Boolean firstClt = true;
            for (ConceptListType clt : vs.getConceptList()) {
                if (firstClt) {
                    clt.setMainList(true);
                    firstClt = false;
                }
                clt.setValueSet(vs);
                for (CE c : clt.getConcept()) {
                    c.setConceptList(clt);
                }
            }

            // Prepare the object
            vs.setId(vs.getId());
            vs.setStatus("Active");
            vs.setSourceURI("");
            vs.setSource("");
            vs.setPurpose("");
            vs.setDefinition("");
            vs.setBinding(BindingType.STATIC);
            vs.setType(TypeType.INTENSIONAL);

            entityManager.persist(vs);
            entityManager.merge(vs);
            entityManager.flush();
        } else {
            // If the import ValueSet contains ConceptList which aren't in the value set database we add them
            Boolean modifications = false;
            for (ConceptListType clt : vs.getConceptList()) {
                Boolean cltExist = false;
                for (ConceptListType cl : exist.getConceptList()) {
                    if (clt.getLang().equalsIgnoreCase(cl.getLang())) {
                        cltExist = true;
                    }
                }
                if (!cltExist) {
                    clt.setValueSet(exist);
                    clt.setMainList(false);
                    for (CE c : clt.getConcept()) {
                        c.setConceptList(clt);
                    }
                    exist.getConceptList().add(clt);
                    modifications = true;
                }
            }

            if (modifications) {
                // Save to BDD
                entityManager.merge(exist);
                entityManager.flush();
            }
        }
    }

    /**
     * Reload the selectedValueSet from the database
     *
     * @param valueSet   Value set to reload
     */
    public void reloadValueSet(DescribedValueSet valueSet) {
        if (valueSet != null) {
            // Reload the selectedValueSet
            HQLQueryBuilder<DescribedValueSet> queryBuilder = new HQLQueryBuilder<DescribedValueSet>(
                    DescribedValueSet.class);
            queryBuilder.addEq("id", valueSet.getId());
            valueSet = queryBuilder.getUniqueResult();
        }
    }

    /**
     * Reload the selectedValueSet from the database
     *
     * @param valueSet   Value set to reload
     * @return DescribedValueSet
     */
    public DescribedValueSet reloadSelectedValueSet(DescribedValueSet valueSet) {
        if (valueSet != null) {
            // Reload the selectedValueSet
            HQLQueryBuilder<DescribedValueSet> queryBuilder = new HQLQueryBuilder<DescribedValueSet>(
                    DescribedValueSet.class);
            queryBuilder.addEq("id", valueSet.getId());
            valueSet = queryBuilder.getUniqueResult();
            entityManager.refresh(valueSet);
            return valueSet;
        }
        return null;
    }

    /**
     * This Method add a concept to all the ConceptList of the selectedValueSet
     *
     * @param concept    Concept to save
     * @param valueSet   Value set to save
     */
    public void addConceptToConceptListOfValueSet(CE concept, DescribedValueSet valueSet) {
        // Add the concept to all concceptList of the VS
        if (valueSet.getConceptList() != null) {
            // Verify if the Concept don't have errors
            Boolean errorValidation = false;
            for (CE c : valueSet.getConceptList().get(0).getConcept()) {
                if (c.getCode().equalsIgnoreCase(concept.getCode())) {
                    FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The code of the new Concept already exists");
                    errorValidation = true;
                }
                if (c.getCode() == null || c.getCode().isEmpty()) {
                    FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The code can't be empty");
                    errorValidation = true;
                }
            }


            // If there's no errors in the Concept we can save it
            if (!errorValidation) {
                valueSet = entityManager.find(DescribedValueSet.class, valueSet.getIdDB());
                for (ConceptListType conceptList : valueSet.getConceptList()) {
                    CE localConcept = new CE(concept);
                    localConcept.setConceptList(conceptList);
                    conceptList.getConcept().add(localConcept);
                }
                entityManager.merge(valueSet);
                entityManager.flush();
            }
        }

        // Prepare for a next concept add in future
        CE saveCE = concept;
        concept = new CE();
        concept.setCodeSystem(saveCE.getCodeSystem());
        concept.setCodeSystemName(saveCE.getCodeSystemName());
        concept.setCodeSystemVersion(saveCE.getCodeSystemVersion());
    }

    /**
     * Save specific concept only
     *
     * @param concept  Concept to save
     */
    public void saveSpecificConcept(CE concept, DescribedValueSet valueSet) {
        CEDAOImpl ceDAOImpl = new CEDAOImpl();
        ceDAOImpl.saveConcept(concept);
        reloadValueSet(valueSet);
    }

    /**
     * Save concept but take care of multiple update for technical rules. The concept is add to ConceptList of the selectedValueSet according to the params
     *
     * @param concept    Concept to save
     * @param saveForAll This attribute allow to save the concept to all the ConceptList of the selectedValueSet
     * @param codeChange This attribute need to be true when this is a code Update
     * @param valueSet  Value set to save
     */
    public void saveSpecificConceptAndAddToValueSet(CE concept, Boolean saveForAll, Boolean codeChange, DescribedValueSet valueSet) {
        // Verify if the Concept don't have errors
        Boolean errorValidation = false;
        CEDAOImpl ceDAOImpl = new CEDAOImpl();

        if (codeChange) {
            for (CE c : valueSet.getConceptList().get(0).getConcept()) {
                if (c.getCode().equalsIgnoreCase(concept.getCode()) && !c.getIdDB().equals(concept.getIdDB())) {
                    FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The code of the modified Concept already exists");
                    errorValidation = true;
                    break;
                }

            }
        }

        if (!errorValidation) {
            // If the modification need to be updated for all concept from the conceptList
            if (saveForAll) {
                CE oldConcept = new CE();
                // For Each ConceptList, update field
                for (ConceptListType clt : valueSet.getConceptList()) {
                    for (CE c : clt.getConcept()) {
                        if (c.getCode().equalsIgnoreCase(oldConcept.getCode())) {
                            if (codeChange) {
                                c.setCode(concept.getCode());
                            } else {
                                c.setCodeSystem(concept.getCodeSystem());
                                c.setCodeSystemVersion(concept.getCodeSystemVersion());
                                c.setCodeSystemName(concept.getCodeSystemName());
                            }
                        }
                    }
                }
                entityManager.merge(valueSet);
                entityManager.flush();
            } else {
                // If not (it's displayName modification) and we just merge the concept
                ceDAOImpl.saveConcept(concept);
            }
        }
    }

}
