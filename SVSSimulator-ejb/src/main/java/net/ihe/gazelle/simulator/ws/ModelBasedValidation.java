package net.ihe.gazelle.simulator.ws;

import net.ihe.gazelle.simulator.statistics.ValidatorUsage;
import net.ihe.gazelle.simulator.svs.action.XSDProviderLocal;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.exception.GazelleValidationException;
import net.ihe.gazelle.validation.model.ValidatorDescription;
import net.ihe.gazelle.validation.ws.AbstractModelBasedValidation;
import net.ihe.svs.GazelleSVSValidator;
import net.ihe.svs.GazelleSVSValidator.ValidatorType;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.xml.soap.SOAPException;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

// do not change those annotations, they are useful to keep a consistency between all validation services 
@Stateless
@Name("ModelBasedValidationWS")
@WebService(name = "ModelBasedValidationWS", serviceName = "ModelBasedValidationWSService", portName = "ModelBasedValidationWSPort", targetNamespace = "http://ws.mb.validator.gazelle.ihe.net")
public class ModelBasedValidation extends AbstractModelBasedValidation {

	@Logger
	private static Log log;

	@Resource
	WebServiceContext webServiceContext;

	/**
	 * Get the list of validators
	 * 
	 * @return List of String
	 */
	@WebMethod
	@WebResult(name = "Validators")
	public List<String> getListOfValidators(@WebParam(name = "descriminator") String descriminator)
			throws SOAPException {
		List<String> validators = new ArrayList<String>();
		for (ValidatorType validator : ValidatorType.values()) {
			validators.add(validator.getLabel());
		}
		return validators;
	}

	/**
	 * Method use to validate a document
	 */
	@WebMethod
	@WebResult(name = "DetailedResult")
	public String validateDocument(@WebParam(name = "document") String document,
			@WebParam(name = "validator") String validator) throws SOAPException {

		HttpServletRequest hRequest = (HttpServletRequest) webServiceContext.getMessageContext().get(MessageContext.SERVLET_REQUEST);
		ValidatorUsage usage = new ValidatorUsage();
		usage.setCaller(hRequest.getRemoteAddr());
		usage.setDate(new Date());
		usage.setType(validator);
		usage.setStatus("NOT PERFORMED");
		if (document == null || document.isEmpty()) {
			usage.save();
			throw new SOAPException("The document to validate appears to be empty.");
		}

		if (validator == null || validator.isEmpty()) {
			usage.save();
			throw new SOAPException("The validator given as parameter is null.");
		}
		ValidatorType validatorType = getValidatorFromValue(validator);
		if (validatorType == null) {
			usage.save();
			throw new SOAPException("The validator seems to not be a valid validator. Refer to this list: "
					+ getListOfValidators(null).toString());
		}

		XSDProviderLocal xsdProvider = (XSDProviderLocal) Component.getInstance("xsdProvider");
		GazelleSVSValidator svsValidator = new GazelleSVSValidator(document, xsdProvider.getSchema(validatorType));

		DetailedResult detailedResult = null;
		try {
			detailedResult = svsValidator.validate(validatorType);
		} catch (Exception e) {
			usage.setStatus("ABORTED");
			usage.save();
			throw new SOAPException(
					"The validator has not been able to process your request correctly due to the following reason: "
							+ e.getMessage());
		}
		if (detailedResult != null) {
			usage.setStatus(detailedResult.getValidationResultsOverview().getValidationTestResult());
			usage.save();
			return svsValidator.getDetailedResultAsString(detailedResult);
		} else {
			return null;
		}
	}

	@Override
	protected String buildReportOnParsingFailure(GazelleValidationException e, ValidatorDescription validatorDescription) {
		return null;
	}

	@Override
	protected String executeValidation(String s, ValidatorDescription validatorDescription, boolean b) throws GazelleValidationException {
		return null;
	}


	@Override
	protected ValidatorDescription getValidatorByOidOrName(String s) {
		return null;
	}

	@Override
	protected List<ValidatorDescription> getValidatorsForDescriminator(String s) {
		return null;
	}

	/**
	 * Get a ValidatorType by a validator name
	 * 
	 * @param validator
	 * @return
	 */
	private ValidatorType getValidatorFromValue(String validator) {
		for (ValidatorType type : ValidatorType.values()) {
			if (type.getLabel().equalsIgnoreCase(validator)) {
				return type;
			}
		}
		return null;
	}
}
