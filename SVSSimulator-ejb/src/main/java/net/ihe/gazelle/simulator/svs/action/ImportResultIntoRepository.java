package net.ihe.gazelle.simulator.svs.action;

import net.ihe.gazelle.gen.common.SVSConsumer;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.simulator.svs.business.UpdatedDescribedValueSet;
import net.ihe.gazelle.simulator.svs.business.ValueSetExtractor;
import net.ihe.gazelle.svs.ConceptListType;
import net.ihe.gazelle.svs.DescribedValueSet;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Synchronized;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <b>ImportResultIntoRepository is the class which contains all operations of the import a result into a repository functionality of SVSimulator.</b>
 *
 * @author nbailliet
 * @version 1.0
 */

@Name("importResultIntoRepository")
@Synchronized(timeout = 350000)
@Scope(ScopeType.PAGE)
public class ImportResultIntoRepository implements Serializable {

    private ValueSetExtractor valueSetExtractor = new ValueSetExtractor();

    private String transactionId;

    private TransactionInstance transactionInstance;

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public TransactionInstance getTransactionInstance() {
        return transactionInstance;
    }

    public void setTransactionInstance(TransactionInstance transactionInstance) {
        this.transactionInstance = transactionInstance;
    }

    @Create
    public void init() {
        Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        transactionId = urlParams.get("id");
        EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
        transactionInstance = entityManager.find(TransactionInstance.class, Integer.parseInt(transactionId));
        try {
            valueSetExtractor.setValidatorTypeAndValidate(transactionInstance.getResponse().getContentAsString(), true, true, false);
        } catch (Exception e) {
            e.printStackTrace();
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Impossible to retrieve Response content: " + e.getMessage());
        }
    }

    public List<DescribedValueSet> getNewImportedValueSetList() {
        return valueSetExtractor.getNewExtractedValueSetList();
    }

    public List<ConceptListType> getNewImportedConceptTypeList() {
        return valueSetExtractor.getNewExtractedConceptTypeList();
    }

    public List<UpdatedDescribedValueSet> getUpdatedImportedValueSetList() {
        return valueSetExtractor.getUpdatedExtractedValueSetList();
    }

    public List<DescribedValueSet> getUpToDateImportedValueSetList() {
        return valueSetExtractor.getUpToDateExtractedValueSetList();
    }
    public List<ConceptListType> getUpToDateImportedConceptList() {
        return valueSetExtractor.getUpToDateExtractedConceptList();
    }


    public List<UpdatedDescribedValueSet> createListFromUpdatedDescribedValueSet(UpdatedDescribedValueSet updatedDescribedValueSet) {
        List<UpdatedDescribedValueSet> updatedDescribedValueSetList = new ArrayList<>();
        if (updatedDescribedValueSet != null) {
            updatedDescribedValueSetList.add(updatedDescribedValueSet);
        }
        return updatedDescribedValueSetList;
    }

    public Boolean hasChangesMade() {
        return valueSetExtractor.hasChangesMade();
    }

    public String back() {
        return "/consumer/svsConsumer.seam";
    }

    public void importToRepository() {
        //Persist data in DB
        try {
            valueSetExtractor.setValidatorTypeAndValidate(transactionInstance.getResponse().getContentAsString(), true, true, true);
            FacesMessages.instance().add(StatusMessage.Severity.INFO, "Changes imported in database.");
        } catch (Exception e) {
            e.printStackTrace();
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Impossible to persist data in database " + e.getMessage());
        }
    }

}
