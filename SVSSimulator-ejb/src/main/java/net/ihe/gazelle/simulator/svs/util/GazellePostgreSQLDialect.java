package net.ihe.gazelle.simulator.svs.util;

import org.hibernate.dialect.PostgreSQLDialect;
import org.hibernate.dialect.function.SQLFunctionTemplate;
import org.hibernate.type.StandardBasicTypes;

public class GazellePostgreSQLDialect extends PostgreSQLDialect {

	public GazellePostgreSQLDialect() {
		super();
		registerFunction("regexp", new SQLFunctionTemplate(StandardBasicTypes.BOOLEAN, "?1 regexp ?2"));
	}

}
