package net.ihe.gazelle.simulator.svs.action;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.svs.GazelleSVSValidator.ValidatorType;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.log.Log;

import javax.ejb.Remove;
import javax.ejb.Startup;
import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.Serializable;

@Startup
@Name("xsdProvider")
@Scope(ScopeType.APPLICATION)
@GenerateInterface("XSDProviderLocal")
public class XSDProvider implements Serializable, XSDProviderLocal {

	/**
	 * 
	 */
	@Logger
	private static Log log;

	private static final long serialVersionUID = 3951653294421040372L;
	private Schema svsSchema;
	private Schema extendedSvsSchema;

	public Schema getSchema(ValidatorType validator) {
		if (validator != null) {
			switch (validator) {
			case ITI48_REQUEST:
				return getSvsSchema();
			case ITI48_RESPONSE:
				return getSvsSchema();
			case ITI60_REQUEST:
				return getExtendedSvsSchema();
			case ITI60_RESPONSE:
				return getExtendedSvsSchema();
			default:
				return null;
			}
		} else {
			log.error("No validator provided");
			return null;
		}
	}

	private Schema getSvsSchema() {
		if (this.svsSchema == null) {
			this.svsSchema = buildSchemaFromFile(ApplicationConfiguration.getValueOfVariable("svs_xsd_location"));
		}
		return this.svsSchema;
	}

	private Schema getExtendedSvsSchema() {
		if (this.extendedSvsSchema == null) {
			this.extendedSvsSchema = buildSchemaFromFile(ApplicationConfiguration
					.getValueOfVariable("esvs_xsd_location"));
		}
		return this.extendedSvsSchema;
	}

	@Remove
	@Destroy
	public void resetSchemas() {
		this.svsSchema = null;
		this.extendedSvsSchema = null;
	}

	private Schema buildSchemaFromFile(String filePath) {
		if (filePath == null) {
			log.error("The path to the XML schema is not available");
			return null;
		} else {
			StreamSource xsd = new StreamSource(filePath);
			Schema schema = null;
			try {
				SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
				schema = factory.newSchema(xsd);
				return schema;
			} catch (Exception e) {
				log.error(e.getMessage(), e);
				return null;
			}
		}
	}

}
