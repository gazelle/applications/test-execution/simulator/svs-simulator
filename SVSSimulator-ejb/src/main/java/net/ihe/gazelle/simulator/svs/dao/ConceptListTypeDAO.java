package net.ihe.gazelle.simulator.svs.dao;

import net.ihe.gazelle.svs.ConceptListType;
import net.ihe.gazelle.svs.DescribedValueSet;

public interface ConceptListTypeDAO {

    void deleteConceptList(DescribedValueSet valueSet, Integer idDB);
    void saveConceptList(DescribedValueSet valueSet, ConceptListType conceptList);
}
