package net.ihe.gazelle.simulator.svs.dao;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.datatypes.CE;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.svs.ConceptListType;
import net.ihe.gazelle.svs.DescribedValueSet;
import net.ihe.gazelle.svs.GroupType;

import java.util.Map;

public interface DescribedValueSetDAO {

    Filter<DescribedValueSet> getFilterForGroup(GroupType group);
    HQLCriterionsForFilter<DescribedValueSet> getHQLCriteriaForFilterForGroup(final GroupType group);
    FilterDataModel<DescribedValueSet> getAllValueSetsForSelectedGroup(GroupType group);

    DescribedValueSet getValueSetFromUrl(Map<String, String> urlParams);
    DescribedValueSet getValueSetfromDB(DescribedValueSet valueSet);
    String saveEditValueSetAndRedirect(DescribedValueSet valueSet);
    void saveEditValueSet(DescribedValueSet valueSet);
    void saveValueSet(DescribedValueSet valueSet);
    String saveValueSetAndRedirect(DescribedValueSet valueSet);
    void deleteValueSet(DescribedValueSet valueSet);
    void deleteConceptFromValueSet(DescribedValueSet valueSet, CE ce);
    Boolean getBoolGroupListEmpty(DescribedValueSet valueSet);
    void addDescribedValueSetFromMultipleValueSetResponse(DescribedValueSet vs);
    void treatDescribedValueSetImportedFromRetrieveValueSetResponse(DescribedValueSet vs);
    void reloadValueSet(DescribedValueSet valueSet);
    DescribedValueSet reloadSelectedValueSet(DescribedValueSet valueSet);
    void addConceptToConceptListOfValueSet(CE concept, DescribedValueSet valueSet);
    void saveSpecificConcept(CE concept, DescribedValueSet valueSet);
    void saveSpecificConceptAndAddToValueSet(CE concept, Boolean saveForAll, Boolean codeChange, DescribedValueSet valueSet);
    void deleteSpecificConceptFromValueSet(DescribedValueSet valueSet, ConceptListType conceptListDB, CE ce);

}
