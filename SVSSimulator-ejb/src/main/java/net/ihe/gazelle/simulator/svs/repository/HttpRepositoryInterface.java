package net.ihe.gazelle.simulator.svs.repository;

import javax.ejb.Local;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBException;

@Local
@Path("/")
public interface HttpRepositoryInterface {

	@GET
	@Produces("text/xml")
	@Path("/RetrieveValueSet")
	public Response retrieveValueSet(@Context HttpServletRequest context) throws JAXBException;

	@GET
	@Produces("text/xml")
	@Path("/RetrieveValueSetForSimulator")
	public Response retrieveValueSetForSimulator(@Context HttpServletRequest context) throws JAXBException;

	@GET
	@Produces("text/xml")
	@Path("/RetrieveMultipleValueSets")
	public Response retrieveMultipleValueSets(@Context HttpServletRequest context);

}
