package net.ihe.gazelle.simulator.svs.repository;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.svs.*;
import org.jboss.seam.annotations.Name;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.SimpleDateFormat;
import java.util.Enumeration;
import java.util.GregorianCalendar;

/**
 * Class manage all HTTP request to the SVS Repository
 *
 * @author mtual
 */
@Stateless
@Name("HttpRepository")
public class HttpRepository implements HttpRepositoryInterface {

    // --------------------- //
    // -- HTTP Repository -- //
    // --------------------- //

    /**
     * Method represent REST Repository for ITI 48 Request
     *
     * @param context HttpServletRequest
     * @return Return a DescribedValueSet in xml form (text/html). In case of error a code error is return.
     * @throws JAXBException jaxb
     */
    public Response retrieveValueSet(HttpServletRequest context) throws JAXBException {
        // Check if invalid parameters in the request and return error 404 with warning message if found it
        // Map<String, String> params = context.getParameterMap();
        RetrieveValueSetRequestType request = new RetrieveValueSetRequestType();
        request.getValueSet().setId(null);
        request.getValueSet().setVersion(null);
        request.getValueSet().setLang(null);
        @SuppressWarnings("unchecked")
        Enumeration<String> params = (Enumeration<String>) context.getParameterNames();
        while (params.hasMoreElements()) {
            String paramName = params.nextElement();
            String value = context.getParameter(paramName);
            if (paramName.equals("id")) {
                request.getValueSet().setId(value);
            } else if (paramName.equals("version")) {
                request.getValueSet().setVersion(value);
            } else if (paramName.equals("lang")) {
                request.getValueSet().setLang(value);
            }
        }
        RepositoryCore core = new RepositoryCore();
        RetrieveValueSetResponseType response = core.buildResponseToRetrieveValueSet(request);
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        core.saveReceivedTransactionInstance(request, response, "ITI-48", context.getRemoteHost(), "HTTP",
                entityManager);
        if (response != null) {
            return Response.ok(SVSMarshaller.marshall(response)).header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_XML+"; charset=UTF-8").build();
        } else if (core.getResponseStatus().equals(RepositoryCore.NAV)) {
            return Response.status(404).header("Warning", "Warning: 111 SVSRepository \"NAV: Unknown value set\"")
                    .build();
        } else if (core.getResponseStatus().equals(RepositoryCore.VERUNK)) {
            return Response.status(404).header("Warning", "Warning: 112 SVSRepository \"VERUNK: Version unknown\"")
                    .build();
        } else {
            return null;
        }
    }

    /**
     * This method return a RetrieveValueSetResponse for Rest Request ITI-48
     *
     * @param context HttpServletRequest
     * @return Return a Response contains RetrieveValueSetResponse (text/xml)
     * @throws JAXBException jaxb
     */
    public Response retrieveValueSetForSimulator(HttpServletRequest context) throws JAXBException {
        RetrieveValueSetRequestType request = new RetrieveValueSetRequestType();
        request.getValueSet().setId(null);
        request.getValueSet().setVersion(null);
        request.getValueSet().setLang(null);
        Boolean random = null;
        String code = null;
        @SuppressWarnings("unchecked")
        Enumeration<String> params = (Enumeration<String>) context.getParameterNames();
        while (params.hasMoreElements()) {
            String paramName = params.nextElement();
            String value = context.getParameter(paramName);
            if (paramName.equals("id")) {
                request.getValueSet().setId(value);
            } else if (paramName.equals("version")) {
                request.getValueSet().setVersion(value);
            } else if (paramName.equals("lang")) {
                request.getValueSet().setLang(value);
            } else if (paramName.equals("random")) {
                random = Boolean.valueOf(value);
            } else if (paramName.equals("code")) {
                code = value;
            }
        }
        RepositoryCore core = new RepositoryCore();
        RetrieveValueSetResponseType response = core.buildResponseForSimulator(request, random, code);
        core.saveToolUsage(context.getRemoteHost());
        Response resp = null;
        if (response != null) {
            resp = Response.ok(SVSMarshaller.marshall(response)).header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_XML+"; charset=UTF-8").build();
        } else {
            resp = Response.status(404).header("Warning", "Warning: 111 SVSRepository \"NAV: Unknown value set\"")
                    .build();
        }

        return resp;
    }

    /**
     * Method represent REST Repository for ITI 60 Request
     */
    public Response retrieveMultipleValueSets(HttpServletRequest context) {
        // Check if invalid parameters
        RetrieveMultipleValueSetsRequestType request = new RetrieveMultipleValueSetsRequestType();
        @SuppressWarnings("unchecked")
        Enumeration<String> params = (Enumeration<String>) context.getParameterNames();
        while (params.hasMoreElements()) {
            String paramName = params.nextElement();
            String value = context.getParameter(paramName);
            if (paramName.equals("id")) {
                request.setID(value);
            } else if (paramName.equals("DisplayNameContains")) {
                request.setDisplayNameContains(value);
            } else if (paramName.equals("SourceContains")) {
                request.setSourceContains(value);
            } else if (paramName.equals("PurposeContains")) {
                request.setPurposeContains(value);
            } else if (paramName.equals("DefinitionContains")) {
                request.setDefinitionContains(value);
            } else if (paramName.equals("GroupContains")) {
                request.setGroupContains(value);
            } else if (paramName.equals("GroupOID")) {
                request.setGroupOID(value);
            } else if (paramName.equals("EffectiveDateBefore")) {
                request.setEffectiveDateBefore(httpDateToXmlGregorianCalendar(value));
            } else if (paramName.equals("EffectiveDateAfter")) {
                request.setEffectiveDateAfter(httpDateToXmlGregorianCalendar(value));
            } else if (paramName.equals("ExpirationDateBefore")) {
                request.setExpirationDateBefore(httpDateToXmlGregorianCalendar(value));
            } else if (paramName.equals("ExpirationDateAfter")) {
                request.setExpirationDateAfter(httpDateToXmlGregorianCalendar(value));
            } else if (paramName.equals("CreationDateBefore")) {
                request.setCreationDateBefore(httpDateToXmlGregorianCalendar(value));
            } else if (paramName.equals("CreationDateAfter")) {
                request.setCreationDateAfter(httpDateToXmlGregorianCalendar(value));
            } else if (paramName.equals("RevisionDateBefore")) {
                request.setRevisionDateBefore(httpDateToXmlGregorianCalendar(value));
            } else if (paramName.equals("RevisionDateAfter")) {
                request.setRevisionDateAfter(httpDateToXmlGregorianCalendar(value));
            } else {
                RepositoryCore core = new RepositoryCore();
                core.setReponseStatus(RepositoryCore.INV);
                EntityManager entityManager = EntityManagerService.provideEntityManager();
                core.saveReceivedTransactionInstance(request, null, "ITI-60", context.getRemoteHost(), "HTTP",
                        entityManager);
                return Response.status(404)
                        .header("Warning", "111 SVSRepository(REST) \"INV: Invalid search parameters\"").build();
            }
        }
        RepositoryCore core = new RepositoryCore();
        RetrieveMultipleValueSetsResponseType response = core.buildResponseToRetrieveMultipleValueSets(request);
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        core.saveReceivedTransactionInstance(request, response, "ITI-60", context.getRemoteHost(), "HTTP",
                entityManager);
        return Response.ok(SVSMarshaller.marshall(response)).header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_XML+"; charset=UTF-8").build();
    }

    private XMLGregorianCalendar httpDateToXmlGregorianCalendar(String httpDate) {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            gregorianCalendar.setTime(formatter.parse(httpDate));
            return DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
        } catch (Exception e) {
            return null;
        }
    }

}
