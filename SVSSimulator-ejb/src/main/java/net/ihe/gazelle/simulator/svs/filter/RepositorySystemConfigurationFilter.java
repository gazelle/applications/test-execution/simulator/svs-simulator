package net.ihe.gazelle.simulator.svs.filter;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.hql.criterion.HQLCriterion;
import net.ihe.gazelle.simulator.svs.model.RepositorySystemConfiguration;

import java.util.ArrayList;
import java.util.List;

public class RepositorySystemConfigurationFilter extends Filter<RepositorySystemConfiguration> {

	private static List<HQLCriterion<RepositorySystemConfiguration, ?>> defaultCriterion;

	static {
		defaultCriterion = new ArrayList<HQLCriterion<RepositorySystemConfiguration, ?>>();
	}

	public RepositorySystemConfigurationFilter() {
		super(RepositorySystemConfiguration.class, defaultCriterion);
	}

}
