package net.ihe.gazelle.simulator.svs.action;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.common.util.XmlFormatter;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.message.action.AbstractTransactionInstanceDisplay;
import net.ihe.gazelle.simulator.message.model.MessageInstance;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.simulator.svs.consumer.RequestSender;
import net.ihe.gazelle.simulator.svs.model.RepositorySystemConfiguration;
import net.ihe.gazelle.simulator.svs.model.ToolUsage;

import net.ihe.gazelle.simulator.svs.model.ToolUsageQuery;
import net.ihe.gazelle.simulator.ws.TestReportImpl;
import net.ihe.gazelle.svs.RetrieveMultipleValueSetsRequestType;
import net.ihe.gazelle.svs.RetrieveValueSetRequestType;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.svs.GazelleSVSValidator;
import net.ihe.svs.GazelleSVSValidator.ValidatorType;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.log.Log;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * This class contains all method for the Messages Display part of the SVS Simulator
 *
 * @author Anne-Gaëlle Bergé / IHE Europe
 */
@Name("transactionInstanceDisplay")
@Scope(ScopeType.PAGE)
public class TransactionInstanceDisplay extends AbstractTransactionInstanceDisplay {

    // ------------------- //
    // ---- Variables ---- //
    // ------------------- //

    private static final long serialVersionUID = -8768529498542226956L;
    @Logger
    private static Log log;
    private String requestContent;
    private String responseContent;
    private String displayedTabType;
    private transient Filter<ToolUsage> filterToolUsage;

    // ------------------------ //
    // ---- Messages Page ----- //
    // ------------------------ //

    private static Element getChildFromSubChildren(Element rootNode, String nodeName) {
        List children = rootNode.getChildren();
        for (Object object : children) {
            Element node = (Element) object;
            if (node.getName().equals(nodeName)) {
                return node;
            }
            Element res = getChildFromSubChildren(node, nodeName);
            if (res != null) {
                return res;
            }
        }
        return null;
    }

    public void initSelectedInstance() {

        EntityManager em = (EntityManager) Component.getInstance("entityManager");
        Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        int idString = Integer.parseInt(urlParams.get("id"));
        selectedInstance = em.find(TransactionInstance.class, idString);

    }
    public  String getTosLink(){
        return ApplicationConfiguration.getValueOfVariable("link_to_cgu");
    }

    /**
     * Method use to get a DetailedResult as a better display
     */
    @Override
    public String getPrettyFormattedResult(MessageInstance messageInstance) {
        // If the transaction have a DetailedResult
        if (messageInstance.getValidationDetailedResult() != null) {
            // We get the xsl path from database
            String xslPath = ApplicationConfiguration.getValueOfVariable("results_xsl_location");
            if (xslPath == null) {
                // if database variable is not set we inform user and return the DetailedResult as String
                FacesMessages
                        .instance()
                        .add("results_xsl_location variable is missing in app_configuration. Should point to the XSL to use for displaying the validation result");
                return new String(messageInstance.getValidationDetailedResult(), StandardCharsets.UTF_8);
            } else {
                // else we transform the DetailedResult with the xsl stylesheet
                return transformXMLStringToHTML(new String(messageInstance.getValidationDetailedResult(),StandardCharsets.UTF_8), xslPath);
            }
        } else{
            return null;
        }
    }


    @Override
    public void validateResponse() {

        XSDProviderLocal xsdProvider = (XSDProviderLocal) Component.getInstance("xsdProvider");
        ValidatorType validatorType = null;
        if (selectedInstance.getResponse().getContent() != null && selectedInstance.getResponse().getContent().length > 0) {
            GazelleSVSValidator validator;
            if (selectedInstance.getResponse().getType() == null || !selectedInstance.getResponse().getType().startsWith("Retrieve")) {
                validatorType = null;
            } else if (selectedInstance.getTransaction().getKeyword().equals("ITI-48")) {
                validatorType = ValidatorType.ITI48_RESPONSE;
            } else {
                validatorType = ValidatorType.ITI60_RESPONSE;
            }
            if (validatorType != null) {
                String response = extractNode(selectedInstance.getResponse().getContent(),
                        validatorType.getLabel().replace(" ", ""));
                validator = new GazelleSVSValidator(response, xsdProvider.getSchema(validatorType));
                try {
                    DetailedResult result = validator.validate(validatorType);
                    selectedInstance.getResponse().setValidationDetailedResult(
                            validator.getDetailedResultAsString(result).getBytes(StandardCharsets.UTF_8));
                    selectedInstance.getResponse().setValidationStatus(
                            result.getValidationResultsOverview().getValidationTestResult());
                    addValidatorUsage(validatorType.getLabel(), result.getValidationResultsOverview()
                            .getValidationTestResult());
                } catch (Exception e) {
                    log.error(e.getMessage());
                    FacesMessages.instance().add("Validation of the response has not ended in an expected way");
                }
            }
        }
        selectedInstance = selectedInstance.save((EntityManager) Component.getInstance("entityManager"));
    }

    @Override
    public void validateRequest() {

        XSDProviderLocal xsdProvider = (XSDProviderLocal) Component.getInstance("xsdProvider");
        ValidatorType validatorType = null;

        if (selectedInstance.getRequest().getContent() != null && selectedInstance.getRequest().getContent().length > 0) {
            GazelleSVSValidator validator;
            if (selectedInstance.getRequest().getType() == null || selectedInstance.getRequest().getType().contains("HTTP")) {
                validatorType = null;
            } else if (selectedInstance.getTransaction().getKeyword().equals("ITI-48")) {
                validatorType = ValidatorType.ITI48_REQUEST;
            } else {
                validatorType = ValidatorType.ITI60_REQUEST;
            }
            if (validatorType != null) {
                String request = extractNode(selectedInstance.getRequest().getContent(),
                        validatorType.getLabel().replace(" ", ""));
                validator = new GazelleSVSValidator(request, xsdProvider.getSchema(validatorType));
                try {
                    DetailedResult result = validator.validate(validatorType);
                    selectedInstance.getRequest().setValidationDetailedResult(
                            validator.getDetailedResultAsString(result).getBytes(StandardCharsets.UTF_8));
                    selectedInstance.getRequest().setValidationStatus(
                            result.getValidationResultsOverview().getValidationTestResult());
                    addValidatorUsage(validatorType.getLabel(), result.getValidationResultsOverview()
                            .getValidationTestResult());
                } catch (Exception e) {
                    log.error(e.getMessage());
                    FacesMessages.instance().add("Validation of the request has not ended in an expected way");
                }
            }
        }
        selectedInstance = selectedInstance.save((EntityManager) Component.getInstance("entityManager"));

    }

    @Override
    public void replayMessage() {
        if (selectedInstance != null && sutForReplay != null){
            send();
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Please select a system in the drop-down list and retry");
        }
    }

    public void send() {

        List<TransactionInstance> transactionsList = null;
        RetrieveValueSetRequestType request48 = new RetrieveValueSetRequestType();
        RetrieveMultipleValueSetsRequestType request60 = new RetrieveMultipleValueSetsRequestType();
        Transaction selectedTransaction = Transaction.GetTransactionByKeyword("ITI-48");

        if (sutForReplay == null) {
            FacesMessages.instance().add("Please select the system under test and retry");
            return;
        }
        RequestSender sender = null;
        if (selectedInstance.getTransaction().getKeyword().equals("ITI-48")) {
            sender = new RequestSender((RepositorySystemConfiguration) sutForReplay, request48, selectedTransaction);
        } else {
            // parse the request, when parameters are empty, set them to null
            if (request60.getID() != null && request60.getID().isEmpty()) {
                request60.setID(null);
            }
            if (request60.getDisplayNameContains() != null && request60.getDisplayNameContains().isEmpty()) {
                request60.setDisplayNameContains(null);
            }
            if (request60.getDefinitionContains() != null && request60.getDefinitionContains().isEmpty()) {
                request60.setDefinitionContains(null);
            }
            if (request60.getSourceContains() != null && request60.getSourceContains().isEmpty()) {
                request60.setSourceContains(null);
            }
            if (request60.getGroupContains() != null && request60.getGroupContains().isEmpty()) {
                request60.setGroupContains(null);
            }
            if (request60.getGroupOID() != null && request60.getGroupOID().isEmpty()) {
                request60.setGroupOID(null);
            }
            if (request60.getPurposeContains() != null && request60.getPurposeContains().isEmpty()) {
                request60.setPurposeContains(null);
            }
            sender = new RequestSender((RepositorySystemConfiguration) sutForReplay, request60, selectedTransaction);
        }
        try {
            TransactionInstance instance = sender.send();
            transactionsList = new ArrayList<TransactionInstance>();
            transactionsList.add(instance);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            FacesMessages.instance().add("Fault message received: " + e.getMessage());
        }
    }

    public void validateResponse(TransactionInstance ti){
        selectedInstance = ti;
        validateResponse();
    }

    public void validateRequest(TransactionInstance ti){
        selectedInstance = ti;
        validateRequest();
    }

    /**
     * Transform a message instance content to xml way with highlight
     *
     * @param messageInstance The object who contain the content to display
     * @return The string xml formatted
     */
    public String displayFormattedXML(MessageInstance messageInstance) {
        // If there'isnt a content, return null
        if (messageInstance.getContent() == null) {
            return null;
        } else {
            // Else pass the string to XmlFormatter
            try {
                return XmlFormatter.format(messageInstance.getContentAsString());
            } catch (Exception e) {
                return messageInstance.getContentAsString();
            }
        }
    }

    /**
     * Get MessageInstance Content as String
     *
     * @param messageInstance message
     * @return String message content
     */
    public String displayAsText(MessageInstance messageInstance) {
        if (messageInstance.getContent() == null) {
            return null;
        } else {
            return messageInstance.getContentAsString();
        }
    }

    /**
     * Transform a message instance content to html way (xsl transformation)
     *
     * @param messageInstance The object who contain the content to display
     * @param typeMessageXSL  Enum type from TransactionInstanceDisplay class, tell the type of message instance Values: {<b>"REQUEST_SOAP"</b>, <b>"RESPONSE_SOAP"</b>, <b>"REQUEST_HTTP"</b>,
     *                        <b>"RESPONSE_HTTP"</b>}
     * @return The string html formatted
     */
    public String displayFormattedHTML(MessageInstance messageInstance, TypeMessage typeMessageXSL) {
        // If there'isnt a content, return null
        if (messageInstance.getContent() == null) {
            return null;
        } else {
            try {
                // Transform the file content using XSL transformation
                String applicationBaseUrl = ApplicationConfiguration.getValueOfVariable("application_url");
                String contentAfterDeleteNamespace = transformXMLStringToHTML(messageInstance.getContentAsString(),
                        applicationBaseUrl.concat("/xsl/deleteNamespace.xsl"));
                // Get the result html (choose the correct xsl)
                String xsl = null;
                switch (typeMessageXSL) {
                    case REQUEST_SOAP:
                        xsl = "/xsl/requestSoapFormater.xsl";
                        break;

                    case RESPONSE_SOAP:
                        xsl = "/xsl/responseFormater.xsl";
                        break;

                    case REQUEST_HTTP:
                        xsl = "/xsl/requestHTTPFormater.xsl";
                        break;

                    case RESPONSE_HTTP:
                        xsl = "/xsl/responseFormater.xsl";
                        break;

                    default:
                        return messageInstance.getContentAsString();
                }
                return transformXMLStringToHTML(contentAfterDeleteNamespace, applicationBaseUrl.concat(xsl));

            } catch (Exception e) {
                return messageInstance.getContentAsString();
            }
        }
    }

    /**
     * Return a link ?
     */
    @Override
    public String permanentLinkToTestReport() {
        return TestReportImpl.buildTestReportRestUrl(selectedInstance.getId(), null);
    }

    /**
     * Method use to validate a Transaction and get a DetailedResult about this validation
     *
     * @param instance The Transaction to validate
     */
    @Override
    public void validate(TransactionInstance instance) {
        selectedInstance = instance;
        validateRequest();
        validateResponse();
        selectedInstance = instance.save((EntityManager) Component.getInstance("entityManager"));

    }

    private String extractNode(byte[] message, String nodeName) {
        SAXBuilder builder = new SAXBuilder();
        XMLOutputter outputter = new XMLOutputter();
        ByteArrayInputStream bais = new ByteArrayInputStream(message);
        Document document = null;
        try {
            document = (Document) builder.build(bais);
        } catch (Exception e) {
            return new String(message,StandardCharsets.UTF_8);
        }
        Element rootNode = document.getRootElement();
        if (rootNode.getName().equals(nodeName)) {
            return new String(message,StandardCharsets.UTF_8);
        }
        Element node = getChildFromSubChildren(rootNode, nodeName);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            outputter.output(node, baos);
        } catch (IOException e) {
            return new String(message,StandardCharsets.UTF_8);
        }
        return new String(baos.toByteArray(),StandardCharsets.UTF_8);
    }

    /**
     * Transform xml string to html string
     *
     * @param documentContent The string xml
     * @param xslPath         The path to the xsl file
     * @return
     */
    private String transformXMLStringToHTML(String documentContent, String xslPath) {
        if (documentContent == null || documentContent.length() == 0){
            return null;
        }

        try {
            if (xslPath == null || xslPath.length() == 0){
                return null;
            }

            Source xmlInput = new StreamSource(new StringReader(documentContent));
            TransformerFactory tFactory = TransformerFactory.newInstance();
            Transformer transformer = tFactory.newTransformer(new StreamSource(xslPath));
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            StreamResult out = new StreamResult(baos);
            transformer.transform(xmlInput, out);
            String tmp = new String(baos.toByteArray(),StandardCharsets.UTF_8);
            return tmp;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return "The document cannot be displayed using this stylesheet";
        }
    }

    /**
     * Allow to know if the MessageDisplay is type SOAP or REST Use to load the correct xsl
     *
     * @return A boolean (true for Soap)
     */
    public Boolean isSoapMessage() {
        if (this.selectedInstance.getRequest().getType().contains("SOAP")) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Method use to get the list of all row ToolUsage (admin interface)
     *
     * @return The list of Tool Usage for fill the DataTable
     */


    // ------------------- //
    // GETTERS AND SETTERS //
    // ------------------- //
    public String getRequestContent() {
        return requestContent;
    }

    // --------------------------------------------------------- //
    // ---- Admin: RetrieveValueSetForSimulator usage Page ----- //
    // --------------------------------------------------------- //

    public void setRequestContent(String requestContent) {
        this.requestContent = requestContent;
    }

    public String getResponseContent() {
        return responseContent;
    }

    public void setResponseContent(String responseContent) {
        this.responseContent = responseContent;
    }

    public String getDisplayedTabType() {
        return displayedTabType;
    }

    public void setDisplayedTabType(String displayedTabType) {
        this.displayedTabType = displayedTabType;
    }

    public String permanentLink(TransactionInstance instance) {
        if (instance != null && instance.getId() != null) {
            String baseURL = ApplicationConfiguration.getValueOfVariable("message_permanent_link");
            if (baseURL != null) {
                return baseURL.concat(instance.getId().toString());
            } else {
                FacesMessages.instance().add("message_permanent_link is missing in app_configuration table");
                return null;
            }
        } else {
            FacesMessages.instance().add("Given instance is either null or has a null id");
            return null;
        }
    }

    public Filter<ToolUsage> getFilterToolUsage() {
        if (filterToolUsage == null) {
            filterToolUsage = new Filter<ToolUsage>(getHQLCriteriaForFilter());
        }
        return filterToolUsage;
    }

    public void setFilterToolUsage(Filter<ToolUsage> filterToolUsage) {
        this.filterToolUsage = filterToolUsage;
    }

    public FilterDataModel<ToolUsage> getAllToolUsages() {
        return new FilterDataModel<ToolUsage>(getFilterToolUsage()) {
            @Override
            protected Object getId(ToolUsage toolUsage) {
                return toolUsage.getId();
            }
        };
    }

    private HQLCriterionsForFilter<ToolUsage> getHQLCriteriaForFilter() {
        ToolUsageQuery query = new ToolUsageQuery();
        HQLCriterionsForFilter<ToolUsage> criteria = query.getHQLCriterionsForFilter();
        criteria.addQueryModifier(new QueryModifier<ToolUsage>() {
            @Override
            public void modifyQuery(HQLQueryBuilder<ToolUsage> hqlQueryBuilder, Map<String, Object> map) {
                //TODO
            }

        });

        return criteria;
    }

    /**
     * Enumeration use to load the correct xsl
     */
    public enum TypeMessage {
        REQUEST_SOAP,
        RESPONSE_SOAP,
        REQUEST_HTTP,
        RESPONSE_HTTP;
    }

}
