package net.ihe.gazelle.simulator.svs.consumer;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.simulator.common.action.ApplicationConfigurationManagerLocal;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.simulator.message.model.MessageInstance;
import net.ihe.gazelle.simulator.svs.action.TransactionInstanceDisplay;
import net.ihe.gazelle.simulator.svs.consumer.ConsumerManagerLocal;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.simulator.svs.model.RepositorySystemConfiguration;
import net.ihe.gazelle.svs.DescribedValueSet;
import net.ihe.gazelle.svs.RetrieveMultipleValueSetsRequestType;
import net.ihe.gazelle.svs.RetrieveValueSetRequestType;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.log.Log;
import org.jboss.seam.security.Identity;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * <b>Consumer Manager is the class which contains all operations of the Consumer from SVS Simulator Application.</b>
 *
 * @author mtual
 * @version 1.0
 */

@Name("consumerManager")
@Synchronized(timeout = 350000)
@Scope(ScopeType.PAGE)
@GenerateInterface("ConsumerManagerLocal")
public class ConsumerManager implements ConsumerManagerLocal, Serializable {

    private static final long serialVersionUID = 1L;

    // ------------------- //
    // ---- Variables ---- //
    // ------------------- //

    private String typeRequest;
    private RepositorySystemConfiguration selectedSUT;
    private Transaction selectedTransaction;
    private RetrieveValueSetRequestType request48;
    private RetrieveMultipleValueSetsRequestType request60;
    private List<TransactionInstance> transactionsList = null;
    private static List<Transaction> svsTransactions;

    static {
        svsTransactions = Transaction.ListAllTransactions();
    }

    // ------------------- //
    // ---- CONSUMER ----- //
    // ------------------- //

    @Create
    public void init() {
        request48 = new RetrieveValueSetRequestType();
        request60 = new RetrieveMultipleValueSetsRequestType();
        selectedTransaction = Transaction.GetTransactionByKeyword("ITI-48");
        selectedSUT = null;
        transactionsList = null;
        typeRequest = "HTTP";
    }

    /**
     * Method load the list of the select item with SUT accorded to role of the user
     *
     * @return Return a list of SelectItem to fill the Select item
     */
    public List<RepositorySystemConfiguration> getListSutType() {
        // Load System Configuration List
        HQLQueryBuilder<RepositorySystemConfiguration> queryBuilder = new HQLQueryBuilder<RepositorySystemConfiguration>(
                RepositorySystemConfiguration.class);
        queryBuilder.addEq("bindingType", typeRequest);
        ApplicationConfigurationManagerLocal manager = (ApplicationConfigurationManagerLocal) Component
                .getInstance("applicationConfigurationManager");
        if (!Identity.instance().isLoggedIn()) {
            queryBuilder.addRestriction(HQLRestrictions.eq("isAvailable", true));
            queryBuilder.addRestriction(HQLRestrictions.eq("isPublic", true));
        } else if (manager.isUserAllowedAsAdmin()) {
            queryBuilder.addRestriction(HQLRestrictions.eq("isAvailable", true));
        } else {
            queryBuilder.addRestriction(HQLRestrictions.eq("isAvailable", true));
            queryBuilder.addRestriction(HQLRestrictions.or(HQLRestrictions.eq("isPublic", true),
                    HQLRestrictions.eq("owner", Identity.instance().getCredentials().getUsername())));
        }
        queryBuilder.addOrder("name", true);
        List<RepositorySystemConfiguration> listSUT = queryBuilder.getList();

        return listSUT;
    }

    /**
     * Reset Parameters SUT
     */
    public void onChangeTypeRequest() {
        selectedSUT = null;
    }

    /**
     * Send a ITI-60 SOAP Request to the selectedSUT Save the transaction in database
     */
    public void send() {
        if (selectedSUT == null) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Please select the system under test and retry");
            return;
        }
        RequestSender sender = null;
        if (selectedTransaction.getKeyword().equals("ITI-48")) {
            sender = new RequestSender(selectedSUT, request48, selectedTransaction);
        } else {
            // parse the request, when parameters are empty, set them to null
            if (request60.getID() != null && request60.getID().isEmpty()) {
                request60.setID(null);
            }
            if (request60.getDisplayNameContains() != null && request60.getDisplayNameContains().isEmpty()) {
                request60.setDisplayNameContains(null);
            }
            if (request60.getDefinitionContains() != null && request60.getDefinitionContains().isEmpty()) {
                request60.setDefinitionContains(null);
            }
            if (request60.getSourceContains() != null && request60.getSourceContains().isEmpty()) {
                request60.setSourceContains(null);
            }
            if (request60.getGroupContains() != null && request60.getGroupContains().isEmpty()) {
                request60.setGroupContains(null);
            }
            if (request60.getGroupOID() != null && request60.getGroupOID().isEmpty()) {
                request60.setGroupOID(null);
            }
            if (request60.getPurposeContains() != null && request60.getPurposeContains().isEmpty()) {
                request60.setPurposeContains(null);
            }
            sender = new RequestSender(selectedSUT, request60, selectedTransaction);
        }
        try {
            TransactionInstance instance = sender.send();
            transactionsList = new ArrayList<TransactionInstance>();
            transactionsList.add(instance);
        } catch (Exception e) {
            e.printStackTrace();
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Fault message received: " + e.getMessage());
        }
    }

    /**
     * Transform a message instance content to html way (xsl transformation)
     *
     * @param transactionInstance The object who contains the response to use
     * @return The string html formatted
     */
    public String displayFormattedHTML(TransactionInstance transactionInstance) {
        // If there'isnt a content, return null
        MessageInstance messageInstance = transactionInstance.getResponse();
        if (messageInstance.getContent() == null) {
            return null;
        } else {
            try {
                // Transform the file content using XSL transformation
                String applicationBaseUrl = ApplicationConfiguration.getValueOfVariable("application_url");
                String contentAfterDeleteNamespace = transformXMLStringToHTML(messageInstance.getContentAsString(),
                        applicationBaseUrl.concat("/xsl/deleteNamespace.xsl"));
                // Get the result html (choose the correct xsl)
                String xsl = "/xsl/responseFormater.xsl";
                return transformXMLStringToHTML(contentAfterDeleteNamespace, applicationBaseUrl.concat(xsl));
            } catch (Exception e) {
                return messageInstance.getContentAsString();
            }
        }

    }

    /**
     * Transform xml string to html string
     *
     * @param documentContent The string xml
     * @param xslPath         The path to the xsl file
     * @return
     */
    @Override
    public String transformXMLStringToHTML(String documentContent, String xslPath) {
        if (documentContent == null || documentContent.length() == 0){
            return null;
        }

        try {
            if (xslPath == null || xslPath.length() == 0){
                return null;
            }

            Source xmlInput = new StreamSource(new StringReader(documentContent));
            TransformerFactory tFactory = TransformerFactory.newInstance();
            Transformer transformer = tFactory.newTransformer(new StreamSource(xslPath));
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            StreamResult out = new StreamResult(baos);
            transformer.transform(xmlInput, out);
            String tmp = new String(baos.toByteArray(), StandardCharsets.UTF_8);
            return tmp;
        } catch (Exception e) {
            return "The content of the Response cannot be displayed.";
        }
    }

    /**
     * Get Redirection link to importResultIntoRepository page for a specific TransactionInstance
     *
     * @param transactionInstance The object who contains the response to use
     * @return page
     */
    public String redirectToImportResultIntoRepository(TransactionInstance transactionInstance) {
        Integer id = transactionInstance.getId();
        if (id != null) {
            return "/browser/importResultIntoRepository.seam?id=" + transactionInstance.getId();
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Impossible to import result !");
            return null;
        }
    }

    // ------------------- //
    // GETTERS AND SETTERS //
    // ------------------- //

    public String getTypeRequest() {
        return typeRequest;
    }

    public void setTypeRequest(String typeRequest) {
        this.typeRequest = typeRequest;
    }

    public RepositorySystemConfiguration getSelectedSUT() {
        return selectedSUT;
    }

    public void setSelectedSUT(RepositorySystemConfiguration selectedSUT) {
        this.selectedSUT = selectedSUT;
    }

    public RetrieveValueSetRequestType getRequest48() {
        return this.request48;
    }

    public void setRequest48(RetrieveValueSetRequestType request48) {
        this.request48 = request48;
    }

    public RetrieveMultipleValueSetsRequestType getRequest60() {
        return request60;
    }

    public void setRequest60(RetrieveMultipleValueSetsRequestType request60) {
        this.request60 = request60;
    }

    public List<TransactionInstance> getTransactionsList() {
        return transactionsList;
    }

    public void setTransactionsList(List<TransactionInstance> transactionsList) {
        this.transactionsList = transactionsList;
    }

    public Transaction getSelectedTransaction() {
        return selectedTransaction;
    }

    public void setSelectedTransaction(Transaction selectedTransaction) {
        this.selectedTransaction = selectedTransaction;
    }

    public List<Transaction> getSvsTransactions() {
        return svsTransactions;
    }
}
