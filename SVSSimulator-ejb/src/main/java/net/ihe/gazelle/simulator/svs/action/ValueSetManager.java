package net.ihe.gazelle.simulator.svs.action;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.datatypes.CE;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.simulator.svs.business.ValueSetExtractor;
import net.ihe.gazelle.simulator.svs.dao.CEDAO;
import net.ihe.gazelle.simulator.svs.dao.ConceptListTypeDAO;
import net.ihe.gazelle.simulator.svs.dao.DescribedValueSetDAO;
import net.ihe.gazelle.simulator.svs.dao.GroupTypeDAO;
import net.ihe.gazelle.simulator.svs.datamodel.DescribedValueSetDataModel;
import net.ihe.gazelle.svs.*;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Synchronized;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import java.io.Serializable;
import java.util.*;

/**
 * <b>Value Set Manager is the class which contains all operations of the SVS Browser from SVS Simulator Application.</b>
 *
 * @author mtual
 * @version 1.0
 */

@Name("valueSetManagerSVS")
@Synchronized(timeout = 350000)
@Scope(ScopeType.PAGE)
public class ValueSetManager implements Serializable {

    private static final long serialVersionUID = 1L;

    @In(create = true, value = "describedValueSetDAO")
    DescribedValueSetDAO describedValueSetDAO;
    @In(create = true, value = "groupTypeDAO")
    GroupTypeDAO groupTypeDAO;
    @In(create = true, value = "conceptListTypeDAO")
    ConceptListTypeDAO conceptListTypeDAO;
    @In(create = true, value = "ceDAO")
    CEDAO ceDAO;

    private ValueSetExtractor valueSetExtractor = new ValueSetExtractor();

    private transient Filter<DescribedValueSet> filter;
    private DescribedValueSet vsExtract;
    private ExtendedFilter requestMVS = new ExtendedFilter();
    private GroupType groupExtract = new GroupType();
    private List<String> listOfStatus;
    private List<String> listLangToggle;
    private List<String> listLangTranslation;
    private DescribedValueSet selectedValueSet;
    private ConceptListType currentConceptList;
    private GroupType selectedGroup;
    private String oldKeyword;
    private String newKeyword = "";
    private CE newConcept;
    private Boolean newConceptBool = false;
    private Boolean displayCurrentVS = false;
    private Boolean translationPanel = false;
    private Boolean boolExtendedFilter = false;
    private Boolean displayAddNewGroup = false;
    private CE selectedConcept;
    private ConceptListType selectedConceptList;

    public ValueSetExtractor getValueSetExtractor() {
        return valueSetExtractor;
    }

    public void setValueSetExtractor(ValueSetExtractor valueSetExtractor) {
        this.valueSetExtractor = valueSetExtractor;
    }

    public DescribedValueSet getSelectedValueSet() {
        return selectedValueSet;
    }

    public void setSelectedValueSet(DescribedValueSet selectedValueSet) {
        this.selectedValueSet = selectedValueSet;
    }

    public List<String> getListOfStatus() {
        return listOfStatus;
    }

    public void setListOfStatus(List<String> listOfStatus) {
        this.listOfStatus = listOfStatus;
    }

    public ConceptListType getCurrentConceptList() {
        if (currentConceptList == null) {
            currentConceptList = new ConceptListType();
        }
        return currentConceptList;
    }

    public void setCurrentConceptList(ConceptListType currentConceptList) {
        this.currentConceptList = currentConceptList;
    }

    public Boolean getNewConceptBool() {
        return newConceptBool;
    }

    public void setNewConceptBool(Boolean newConceptBool) {
        this.newConceptBool = newConceptBool;
        if (newConceptBool) {
            this.translationPanel = false;
        }
    }

    public CE getNewConcept() {
        if (newConcept == null) {
            this.newConcept = new CE();
        }
        return newConcept;
    }

    public void setNewConcept(CE newConcept) {
        this.newConcept = newConcept;
    }

    public Boolean getDisplayCurrentVS() {
        return displayCurrentVS;
    }

    public void setDisplayCurrentVS(Boolean displayCurrentVS) {
        this.displayCurrentVS = displayCurrentVS;
    }

    public DescribedValueSet getVsExtract() {
        if (vsExtract == null) {
            vsExtract = new DescribedValueSet();
        }
        return vsExtract;
    }

    public void setVsExtract(DescribedValueSet vsExtract) {
        this.vsExtract = vsExtract;
    }

    public List<String> getListLangToggle() {
        if (listLangToggle == null) {
            listLangToggle = new ArrayList<String>();
        }
        return listLangToggle;
    }

    public void setListLangToggle(List<String> listLangToggle) {
        this.listLangToggle = listLangToggle;
    }

    public Boolean getTranslationPanel() {
        return translationPanel;
    }

    public void setTranslationPanel(Boolean translationPanel) {
        this.translationPanel = translationPanel;
        if (translationPanel) {
            this.newConceptBool = false;
        }
    }

    public List<String> getListLangTranslation() {
        if (listLangTranslation == null) {
            listLangTranslation = new ArrayList<String>();
        }
        return listLangTranslation;
    }

    public void setListLangTranslation(List<String> listLangTranslation) {
        this.listLangTranslation = listLangTranslation;
    }

    public GroupType getSelectedGroup() {
        return selectedGroup;
    }

    public void setSelectedGroup(GroupType selectedGroup) {
        this.selectedGroup = selectedGroup;
    }

    public ExtendedFilter getRequestMVS() {
        if (requestMVS == null) {
            requestMVS = new ExtendedFilter();
        }
        return requestMVS;
    }

    public void setRequestMVS(ExtendedFilter requestMVS) {
        this.requestMVS = requestMVS;
    }

    public Boolean getBoolExtendedFilter() {
        return boolExtendedFilter;
    }

    public void setBoolExtendedFilter(Boolean boolExtendedFilter) {
        this.boolExtendedFilter = boolExtendedFilter;
    }

    public String getNewKeyword() {
        return newKeyword;
    }

    public void setNewKeyword(String newKeyword) {
        this.newKeyword = newKeyword;
    }

    public Boolean getDisplayAddNewGroup() {
        return displayAddNewGroup;
    }

    public void setDisplayAddNewGroup(Boolean displayAddNewGroup) {
        this.displayAddNewGroup = displayAddNewGroup;
    }

    public GroupType getGroupExtract() {
        return groupExtract;
    }

    public void setGroupExtract(GroupType groupExtract) {
        this.groupExtract = groupExtract;
    }

    public String getOldKeyword() {
        return oldKeyword;
    }

    public void setOldKeyword(String oldKeyword) {
        this.oldKeyword = oldKeyword;
        this.newKeyword = oldKeyword;
    }

    public ConceptListType getSelectedConceptList() {
        return selectedConceptList;
    }

    public void setSelectedConceptList(ConceptListType selectedConceptList) {
        this.selectedConceptList = selectedConceptList;
    }

    public void addConceptToList() {
        describedValueSetDAO.addConceptToConceptListOfValueSet(newConcept, selectedValueSet);
        setNewConceptBool(false);
        setListLangToggle(new ArrayList<String>());
    }

    public CE getSelectedConcept() {
        return selectedConcept;
    }

    public void setSelectedConcept(CE selectedConcept) {
        this.selectedConcept = selectedConcept;
    }

    /**
     * Add the created Concept List to the selectedValueSet and Save the selectedValueSet in database
     *
     * @return Return the redirection link
     */
    public String saveConceptList() {
        // First, test if there's already a Concept List with this lang
        // If yes, generate other name for the ConceptList
        Integer count = 1;
        Boolean exist;
        if (currentConceptList.getLang() == null || currentConceptList.getLang().isEmpty()) {
            FacesMessages.instance().add("The language of the concept list cannot be empty !");
            return null;
        } else {
            String saveLang = currentConceptList.getLang();
            do {
                exist = false;
                for (ConceptListType clt : selectedValueSet.getConceptList()) {
                    if (clt.getLang() != null && clt.getLang().equalsIgnoreCase(currentConceptList.getLang())) {
                        exist = true;
                        count++;
                        currentConceptList.setLang((saveLang + "." + count));
                    }
                }
            } while (exist);

            setListLangToggle(new ArrayList<String>());
            selectedValueSet.getConceptList().add(currentConceptList);
            String reload = describedValueSetDAO.saveEditValueSetAndRedirect(selectedValueSet);
            this.currentConceptList = new ConceptListType();
            return reload;
        }
    }

    public void saveSpecificConcept(CE concept, Boolean saveForAll, Boolean codeChange) {
        describedValueSetDAO.saveSpecificConceptAndAddToValueSet(concept, saveForAll, codeChange, selectedValueSet);
    }

    /**
     * Save selected conceptList
     */
    public void saveSelectedConceptList(ConceptListType conceptListType) {
        conceptListTypeDAO.saveConceptList(selectedValueSet, conceptListType);
    }

    /**
     * This method return the types available for a ValueSet
     *
     * @return Return a list of item for Select
     */
    public List<SelectItem> getAvailableTypesAsItem() {
        TypeType[] types = TypeType.values();
        List<SelectItem> items = new ArrayList<SelectItem>();
        for (TypeType type : types) {
            items.add(new SelectItem(type, type.value()));
        }
        return items;
    }

    /**
     * This method return the binding available for a ValueSet
     *
     * @return Return a list of item for Select
     */
    public List<SelectItem> getAvailableBindingsAsItem() {
        BindingType[] bindings = BindingType.values();
        List<SelectItem> items = new ArrayList<SelectItem>();
        for (BindingType binding : bindings) {
            items.add(new SelectItem(binding, binding.value()));
        }
        return items;
    }

    /**
     * Get Redirection link to displayValueSet page for a specific Value Set
     *
     * @param valueSet The valueSet where the link point
     * @return page
     */
    public String displayValueSet(DescribedValueSet valueSet) {
        return "/browser/valueSet.seam?id=" + valueSet.getId();
    }

    /**
     * Get Redirection link to editValueSet page for a specific Value Set
     *
     * @param valueSet The valueSet where the link point
     * @return page
     */
    public String editValueSet(DescribedValueSet valueSet) {
        return "/browser/editValueSet.seam?id=" + valueSet.getId();
    }

    /**
     * Get REST request for a specific id
     *
     * @param id The OID of the Value Set to Retrieve
     * @return Redirection String
     */
    public String retrieveValueSet(String id) {
        return "/SVSSimulator/rest/RetrieveValueSetForSimulator?id=" + id;
    }

    /**
     * Get REST request for a specific group OID
     *
     * @param id The OID of the Group to Retrieve
     * @return Redirection String
     */
    public String retrieveValueSetGroup(String id) {
        return "/SVSSimulator/rest/RetrieveMultipleValueSets?GroupOID=" + id;
    }

    /**
     * This method is called at the loading. Retrieve from the database the value Set associate to the url id. Order the different lists of the value Set for display.
     */
    public void getValueSetFromUrl() {
        // If there's an id in the URL, get it and load the selectedValueSet
        Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (urlParams != null && urlParams.containsKey("id")) {
            try {
                this.selectedValueSet = describedValueSetDAO.getValueSetFromUrl(urlParams);

                this.currentConceptList = new ConceptListType();
                if (this.selectedValueSet == null) {
                    this.selectedValueSet = new DescribedValueSet();
                } else {
                    if (selectedValueSet.getConceptList() != null) {
                        ConceptListType mainList = null;
                        for (ConceptListType clt : selectedValueSet.getConceptList()) {
                            if (clt.getMainList() != null && clt.getMainList()) {
                                mainList = clt;
                                break;
                            }
                        }
                        if (mainList != null) {
                            openedConceptList(mainList.lang);
                        }
                    }
                }
            } catch (Exception e) {
                this.selectedValueSet = new DescribedValueSet();
            }
        } else // Else we created a new value set
        {
            this.selectedValueSet = new DescribedValueSet();
        }

        // Order by lang all the concept List
        List<ConceptListType> conceptListOrderByLang = this.selectedValueSet.getConceptList();
        Collections.sort(conceptListOrderByLang, new Comparator<ConceptListType>() {
            @Override
            public int compare(ConceptListType o1, ConceptListType o2) {
                if (o1.getLang() == null) {
                    return 0;
                } else {
                    return o1.getLang().compareToIgnoreCase(o2.getLang());
                }
            }
        });

        // Put the mainList in top of the list
        Integer iterator = 0;
        Integer savePosition = null;
        for (ConceptListType clt : conceptListOrderByLang) {
            if (clt.getMainList()) {
                savePosition = iterator;
            }
            iterator++;
        }

        if (savePosition != null) {
            ConceptListType c = conceptListOrderByLang.get(0);
            conceptListOrderByLang.add(0, conceptListOrderByLang.get(savePosition));
            conceptListOrderByLang.remove(1);
            conceptListOrderByLang.add(savePosition, c);
            conceptListOrderByLang.remove((savePosition + 1));
        }

        // Order by code all the concept for each conceptList of the valueSet
        List<ConceptListType> conceptList = this.selectedValueSet.getConceptList();
        for (ConceptListType clt : conceptList) {
            List<CE> list = clt.concept;
            Collections.sort(list, new Comparator<CE>() {
                @Override
                public int compare(CE o1, CE o2) {
                    return o1.getCode().compareToIgnoreCase(o2.getCode());
                }
            });
        }

        // Set the params for the newConcept object
        if (selectedValueSet.getConceptList().size() > 0) {
            if (selectedValueSet.getConceptList().get(0).getConcept().size() > 0) {
                getNewConcept().setCodeSystem(
                        selectedValueSet.getConceptList().get(0).getConcept().get(0).getCodeSystem());
                getNewConcept().setCodeSystemVersion(
                        selectedValueSet.getConceptList().get(0).getConcept().get(0).getCodeSystemVersion());
                getNewConcept().setCodeSystemName(
                        selectedValueSet.getConceptList().get(0).getConcept().get(0).getCodeSystemName());
            }
        }
    }

    /**
     * Create a new ConceptList but don't add it to the selectedValueSet (preparation)
     */
    public void createNewConceptList() {
        // If the list of conceptList is null we build a new one and assign it to the current VS
        if (selectedValueSet.getConceptList() == null || selectedValueSet.getConceptList().size() == 0) {
            currentConceptList = new ConceptListType();
            currentConceptList.setMainList(true);
            currentConceptList.setValueSet(selectedValueSet);
        } else // Else we built a new one too but with with same params than the main conceptList
        {
            ConceptListType mainConceptList = null;
            for (ConceptListType conceptList : selectedValueSet.getConceptList()) {
                if (conceptList.getMainList() != null && conceptList.getMainList()) {
                    mainConceptList = conceptList;
                    break;
                }
            }
            // We build a new conceptList with params of the main ConceptList
            currentConceptList = new ConceptListType(mainConceptList);
            currentConceptList.setMainList(false);
            currentConceptList.setValueSet(selectedValueSet);
            for (CE ce : currentConceptList.getConcept()) {
                ce.setConceptList(currentConceptList);
            }
        }

        // Reset display opened list
        setListLangToggle(new ArrayList<String>());
    }

    /**
     * Delete selected conceptList from DB
     */
    public void deleteSelectedConceptList() {
        conceptListTypeDAO.deleteConceptList(selectedValueSet,selectedConceptList.getIdDB());
    }

    /**
     * Delete selected Concept from value set
     */
    public void deleteSelectedConcept() {
        describedValueSetDAO.deleteConceptFromValueSet(selectedValueSet,selectedConcept);

    }

    /**
     * Delete selected value set from DB
     */
    public String deleteSelectedValueSet() {
        describedValueSetDAO.deleteValueSet(selectedValueSet);
        return "/browser/valueSetBrowser.seam";
    }

    /**
     * Allow to know if a toggle panel need to be opened or closed
     *
     * @param clt The ConceptList to test if opened or not
     * @return True or False
     */
    public Boolean getOpenedToggle(ConceptListType clt) {
        // Verify if it's a main conceptList
        if (clt.getMainList()) {
            return true;
        }

        // Verify if the conceptList is in the list of opened language
        for (String language : getListLangToggle()) {
            if (language.equalsIgnoreCase(clt.getLang())) {
                return true;
            }
        }

        return false;
    }

    /**
     * Set a conceptList to "opened"
     *
     * @param lang The lang of the Concept List to opened
     */
    public void openedConceptList(String lang) {
        getListLangToggle().add(lang);
    }

    /**
     * Use to manage language display in translation panel
     *
     * @param lang The lang to add or remove from the list of translation opened
     */
    public void changeLangTranslation(String lang) {
        // Add or Remove lang in the list containing the display language for translate panel
        if (getListLangTranslation().contains(lang)) {
            getListLangTranslation().remove(lang);
        } else {
            getListLangTranslation().add(lang);
        }

        describedValueSetDAO.reloadValueSet(selectedValueSet);
    }

    /**
     * Get Translation Display Exist for boolean display in translation panel
     *
     * @param lang The lang to test if exist
     * @return Boolean
     */
    public Boolean getTDExist(String lang) {
        return getListLangTranslation().contains(lang);
    }

    /**
     * Allow to load the object selectedGroup accorded to URL
     */
    public void getGroupFromUrl() {
        // If there's an id in the URL, get it and load the selectedGroup
        Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (urlParams != null && urlParams.containsKey("id")) {
            try {
                // Fill the selectedGroup with database record
                this.selectedGroup = groupTypeDAO.getGroupTypeFromUrl(urlParams);

                if (this.selectedGroup == null) {
                    this.selectedGroup = new GroupType();
                }
            } catch (Exception e) {
                this.selectedGroup = new GroupType();
            }
        } else // Else create a new Group
        {
            this.selectedGroup = new GroupType();
        }
    }

    /**
     * Save new keyword and remvoe old one
     */
    public void saveEditKeyword() {
        if (this.newKeyword != null && !this.newKeyword.isEmpty()) {
            this.selectedGroup.getKeyword().remove(this.oldKeyword);
            this.selectedGroup.getKeyword().add(this.newKeyword);
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The keyword must not be empty");
        }
    }

    /**
     * Add a keyword for the selectedGroup
     */
    public void addKeyWordGroup() {
        if (this.newKeyword != null && !this.newKeyword.isEmpty()) {
            this.selectedGroup.addKeyword(this.newKeyword);
            this.newKeyword = "";
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The keyword must not be empty");
        }
    }

    /**
     * This method return a link for the page group (view/edit)
     *
     * @param valueSet The valueSet where the link pointed
     * @return Value set overview page URL
     */
    public String displayValueSetForGroup(DescribedValueSet valueSet) {
        return "/browser/valueSet.seam?id=" + valueSet.getId();
    }

    /**
     * Get the redirection link to display page for specific group
     *
     * @param group Specific group to display
     * @return Group overview page URL
     */
    public String displayGroup(GroupType group) {
        return "/browser/group.seam?id=" + group.getID();
    }

    /**
     * Get the redirection link to edit page for specific group
     *
     * @param group Group to edit
     * @return Group edition page URL
     */
    public String editGroup(GroupType group) {
        return "/browser/editGroup.seam?id=" + group.getID();
    }


    public String deleteSelectedGroup() {
        return groupTypeDAO.deleteGroup(this.selectedGroup);
    }

    /**
     * Allow to load only valueSet which not already in the group
     */
    public void prepareListValueSets() {
        this.requestMVS.notGroupOID = this.selectedGroup.getID();
    }

    /**
     * Return if value set has any group or not
     *
     * @param valueSet Value set to test
     */
    public Boolean getBoolGroupListEmpty (DescribedValueSet valueSet) {
        return describedValueSetDAO.getBoolGroupListEmpty(valueSet);
    }

    /**
     * Save selected value set and redirect to its edition page
     */
    public String saveValueSet() {
        if (selectedValueSet != null) {
            return describedValueSetDAO.saveValueSetAndRedirect(selectedValueSet);
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Unable to redirect to Value Set edition page");
            return null;
        }
    }

    /**
     * Save edited value set and redirect to its edition page
     */
    public void saveEditValueSet() {
        if (selectedValueSet != null) {
            describedValueSetDAO.saveEditValueSet(selectedValueSet);
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Unable to redirect to Value set edition page");
        }
    }

    /**
     * Save selected group and redirect to its edition page
     */
    public String saveGroup() {
        if (selectedGroup != null) {
            return groupTypeDAO.saveGroupAndRedirect(selectedGroup);
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Unable to redirect to Group edition page");
            return null;
        }
    }

    /**
     * Save edited group and redirect to its edition page
     */
    public void saveEditGroup() {
        if (selectedGroup != null) {
            groupTypeDAO.saveEditGroup(selectedGroup);
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Unable to redirect to Group edition page");
        }
    }

    /**
     * Add link between the selected group and a value set
     *
     * @param valueSet valueSet
     */
    public void addValueSetToGroup(DescribedValueSet valueSet) {
        if (valueSet != null && selectedGroup != null) {
            groupTypeDAO.addValueSetToGroup(valueSet,selectedGroup);
        }
    }

    /**
     * Remove the link between the selected group and a value set
     *
     * @param valueSet valueSet
     */
    public void removeValueSetToGroup(DescribedValueSet valueSet) {
        if (valueSet != null && selectedGroup != null) {
            groupTypeDAO.removeValueSetToGroup(valueSet,selectedGroup);
        }
    }

    public Filter<DescribedValueSet> getFilter() {
        if (filter == null) {
            filter = new Filter<DescribedValueSet>(getHQLCriteriaForFilter());
        }
        return filter;
    }

    public void setFilter(Filter<DescribedValueSet> filter) {
        this.filter = filter;
    }

    public HQLCriterionsForFilter<DescribedValueSet> getHQLCriteriaForFilter() {
        DescribedValueSetQuery query = new DescribedValueSetQuery();
        HQLCriterionsForFilter<DescribedValueSet> criteria = query.getHQLCriterionsForFilter();
        criteria.addPath("purpose", query.purpose());
        criteria.addPath("effectiveDate", query.effectiveDateUtil());
        criteria.addPath("creationDate", query.creationDateUtil());
        criteria.addPath("expirationDate", query.expirationDateUtil());
        criteria.addPath("revisionDate", query.revisionDateUtil());
        criteria.addPath("groupOid", query.group().iD());

        criteria.addQueryModifier(new QueryModifier<DescribedValueSet>() {
            @Override
            public void modifyQuery(HQLQueryBuilder<DescribedValueSet> hqlQueryBuilder, Map<String, Object> map) {

            }
        });

        return criteria;
    }

    public void resetFilter() {
        getFilter().clear();
    }


    public FilterDataModel<DescribedValueSet> getAllValueSets() {

        return new FilterDataModel<DescribedValueSet>(getFilter()) {
            @Override
            protected Object getId(DescribedValueSet describedValueSet) {
                return describedValueSet.getId();
            }

            @Override
            public DescribedValueSet getRowData() {
                if (this.getDataItem() != null) {
                    HQLQueryBuilder queryBuilder;
                    if (this.getEntityManager() != null) {
                        queryBuilder = new HQLQueryBuilder(this.getEntityManager(), this.getDataClass());
                    } else {
                        queryBuilder = new HQLQueryBuilder(this.getDataClass());
                    }
                    queryBuilder.addEq("id", this.getDataItem());
                    return (DescribedValueSet) queryBuilder.getUniqueResult();
                }
                return null;
            }
        };
    }

    public FilterDataModel<GroupType> getAllGroups() {
        return groupTypeDAO.getAllGroups();
    }

    public Filter<DescribedValueSet> getFilterForGroup() {
        return describedValueSetDAO.getFilterForGroup(selectedGroup);
    }

    public FilterDataModel<DescribedValueSet> getAllValueSetsForSelectedGroup() {
        return describedValueSetDAO.getAllValueSetsForSelectedGroup(selectedGroup);
    }

    /**
     * Get the list of Value Set for browser (filter)
     *
     * @return list of Value Set (fill datatable)
     */
    public DescribedValueSetDataModel getValueSets() {
        return new DescribedValueSetDataModel(getFilter(), this.vsExtract, this.requestMVS);
    }

    public boolean groupIsNotLinkedToValueSet(GroupType groupType) {
        return groupType.getValueSets().isEmpty();
    }

    public void initCreateGroup() {
        this.selectedGroup = new GroupType();
    }

    public void initCreateValueSet() {
        this.selectedValueSet = new DescribedValueSet();
    }

    public void downloadValueSetsFromGroup() {
        if (selectedGroup != null) {
            valueSetExtractor.downloadValueSets(selectedGroup.getValueSets());
        }
    }

    public void downloadValueSetsFromGroup(GroupType gt) {
        valueSetExtractor.downloadValueSets(gt.getValueSets());
    }

    /**
     * Class use to stock extended input in the filter of valueSetBrowser Send to DataModel to get list of Value Set
     *
     * @author mtual
     */
    public class ExtendedFilter {
        private Date effectiveDateBefore;
        private Date effectiveDateAfter;
        private Date expirationDateBefore;
        private Date expirationDateAfter;
        private Date revisionDateBefore;
        private Date revisionDateAfter;
        private Date creationDateBefore;
        private Date creationDateAfter;
        private String groupOID;
        private String groupContains;
        private String notGroupOID;

        public ExtendedFilter() {
        }

        public Date getEffectiveDateBefore() {
            return effectiveDateBefore;
        }

        public void setEffectiveDateBefore(Date effectiveDateBefore) {
            this.effectiveDateBefore = effectiveDateBefore;
        }

        public Date getEffectiveDateAfter() {
            return effectiveDateAfter;
        }

        public void setEffectiveDateAfter(Date effectiveDateAfter) {
            this.effectiveDateAfter = effectiveDateAfter;
        }

        public Date getExpirationDateBefore() {
            return expirationDateBefore;
        }

        public void setExpirationDateBefore(Date expirationDateBefore) {
            this.expirationDateBefore = expirationDateBefore;
        }

        public Date getExpirationDateAfter() {
            if (expirationDateAfter == null) {
                return null;
            } else {
                return (Date) expirationDateAfter.clone();
            }
        }

        public void setExpirationDateAfter(Date expirationDateAfter) {
            this.expirationDateAfter = expirationDateAfter;
        }

        public Date getRevisionDateBefore() {
            return revisionDateBefore;
        }

        public void setRevisionDateBefore(Date revisionDateBefore) {
            this.revisionDateBefore = revisionDateBefore;
        }

        public Date getRevisionDateAfter() {
            return revisionDateAfter;
        }

        public void setRevisionDateAfter(Date revisionDateAfter) {
            this.revisionDateAfter = revisionDateAfter;
        }

        public Date getCreationDateBefore() {
            return creationDateBefore;
        }

        public void setCreationDateBefore(Date creationDateBefore) {
            this.creationDateBefore = creationDateBefore;
        }

        public Date getCreationDateAfter() {
            return creationDateAfter;
        }

        public void setCreationDateAfter(Date creationDateAfter) {
            this.creationDateAfter = creationDateAfter;
        }

        public String getGroupOID() {
            return groupOID;
        }

        public void setGroupOID(String groupOID) {
            this.groupOID = groupOID;
        }

        public String getGroupContains() {
            return groupContains;
        }

        public void setGroupContains(String groupContains) {
            this.groupContains = groupContains;
        }

        public String getNotGroupOID() {
            return notGroupOID;
        }

        public void setNotGroupOID(String notGroupOID) {
            this.notGroupOID = notGroupOID;
        }
    }
}