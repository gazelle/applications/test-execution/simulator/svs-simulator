package net.ihe.gazelle.simulator.svs.consumer;

import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.simulator.svs.model.RepositorySystemConfiguration;
import net.ihe.gazelle.simulator.svs.repository.RepositoryCore;
import net.ihe.gazelle.simulator.svs.repository.SoapRepository.NAVFault;
import net.ihe.gazelle.simulator.svs.repository.SoapRepository.VERUNKFault;
import net.ihe.gazelle.svs.*;
import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;
import org.jboss.seam.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.ws.rs.core.MultivaluedMap;
import java.net.MalformedURLException;
import java.util.Date;

public class RequestSender {

	private static Logger log = LoggerFactory.getLogger(RequestSender.class);
	private static final String ITI48_ACTION = "urn:ihe:iti:2008:RetrieveValueSet";
	private static final String ITI60_ACTION = "urn:ihe:iti:2010:RetrieveMultipleValueSets";

	private RepositorySystemConfiguration sut;
	private Object request;
	private Transaction simulatedTransaction;
	private String responseStatus;

	public RequestSender(RepositorySystemConfiguration sut, Object inRequest, Transaction inTransaction) {
		this.sut = sut;
		this.request = inRequest;
		this.simulatedTransaction = inTransaction;
	}

	public TransactionInstance send(){
		TransactionInstance instance = new TransactionInstance();
		instance.setDomain(Domain.getDomainByKeyword("ITI"));
		instance.setTimestamp(new Date());
		Actor consumer = Actor.findActorWithKeyword("VALUE_SET_CONSUMER");
		instance.setSimulatedActor(consumer);
		instance.setTransaction(simulatedTransaction);
		instance.getRequest().setIssuingActor(consumer);
		instance.getResponse().setIssuingActor(Actor.findActorWithKeyword("VALUE_SET_REPOSITORY"));
		instance.getRequest().setIssuer("SVS Simulator");
		instance.getResponse().setIssuer(sut.getName());
		instance.getRequest().setContent(SVSMarshaller.marshall(request));
		if (sut.getBindingType().equalsIgnoreCase("SOAP")) {
			instance.getResponse().setContent(sendSOAPRequest());
		} else {
			try {
				instance.getResponse().setContent(sendRESTRequest());
			} catch (Exception e) {
				log.error(e.getMessage(), e);
			}
		}
		if (simulatedTransaction.getKeyword().equals("ITI-48")) {
			instance.getRequest().setType("RetrieveValueSetRequest (" + sut.getBindingType() + ")");
			if (responseStatus != null) {
				instance.getResponse().setType(responseStatus);
			} else {
				instance.getResponse().setType("RetrieveValueSetResponse (" + sut.getBindingType() + ")");
			}
		} else {
			instance.getRequest().setType("RetrieveMultipleValueSetsRequest (" + sut.getBindingType() + ")");
			instance.getResponse().setType("RetrieveMultipleValueSetsResponse (" + sut.getBindingType() + ")");
		}
		instance = instance.save((EntityManager) Component.getInstance("entityManager"));
		return instance;
	}

	private String sendSOAPRequest() {
		String xmlResponse;
		SOAPRequestSender soapRequestSender = new SOAPRequestSender();
		soapRequestSender.setSutUrl(this.sut.getUrl());
		if (simulatedTransaction.getKeyword().equals("ITI-48")) {
			try {
				RetrieveValueSetRequestType requestType = (RetrieveValueSetRequestType) request;
				RetrieveValueSetResponseType response = soapRequestSender.send(RetrieveValueSetResponseType.class,
						requestType.get_xmlNodePresentation(), ITI48_ACTION);
				xmlResponse = SVSMarshaller.marshall(response);
				responseStatus = null;
			} catch (NAVFault fault) {
				responseStatus = RepositoryCore.NAV;
				xmlResponse = fault.getFault().getFaultString();
			} catch (VERUNKFault fault) {
				responseStatus = RepositoryCore.VERUNK;
				xmlResponse = fault.getFault().getFaultString();
			} catch (Exception exception){
				xmlResponse = exception.getMessage();
				responseStatus = null;
			}
		} else {
			RetrieveMultipleValueSetsRequestType requestType = (RetrieveMultipleValueSetsRequestType) request;
			try {
				RetrieveMultipleValueSetsResponseType response = soapRequestSender.send(RetrieveMultipleValueSetsResponseType.class,
						requestType.get_xmlNodePresentation(), ITI60_ACTION);
				xmlResponse = SVSMarshaller.marshall(response);
			}catch (Exception e){
				xmlResponse = e.getMessage();
				responseStatus = null;
			}
		}
		return xmlResponse;
	}

	private String sendRESTRequest() {


		StringBuilder endpoint = new StringBuilder(sut.getEndpoint());
		if (sut.getEndpoint().lastIndexOf("/") != (sut.getEndpoint().length() - 1)) // If
		{
			endpoint.append("/");
		}
		ClientRequest httpClient = null;
		if (simulatedTransaction.getKeyword().equals("ITI-48")) {
			endpoint.append("RetrieveValueSet");
			try {
				httpClient = new ClientRequest(endpoint.toString());
				RetrieveValueSetRequestType iti48Request = (RetrieveValueSetRequestType) request;
				if (iti48Request.getValueSet().getId() != null && !iti48Request.getValueSet().getId().isEmpty()) {
					httpClient.queryParameter("id", iti48Request.getValueSet().getId());
				}
				if (iti48Request.getValueSet().getVersion() != null
						&& !iti48Request.getValueSet().getVersion().isEmpty()) {
					httpClient.queryParameter("version", iti48Request.getValueSet().getVersion());
				}
				if (iti48Request.getValueSet().getLang() != null && !iti48Request.getValueSet().getLang().isEmpty()) {
					httpClient.queryParameter("lang", iti48Request.getValueSet().getLang());
				}
			} catch (RuntimeException e) {
				responseStatus = null;
				return null;
			}
		} else {
			endpoint.append("RetrieveMultipleValueSets");
			try {
				httpClient = new ClientRequest(endpoint.toString());
				RetrieveMultipleValueSetsRequestType iti60Request = (RetrieveMultipleValueSetsRequestType) request;
				// Set the request Parameters
				if (iti60Request.getID() != null && !iti60Request.getID().isEmpty()) {
					httpClient.queryParameter("id", iti60Request.getID());
				}
				if (iti60Request.getDisplayNameContains() != null && !iti60Request.getDisplayNameContains().isEmpty()) {
					httpClient.queryParameter("DisplayNameContains", iti60Request.getDisplayNameContains());
				}
				if (iti60Request.getSourceContains() != null && !iti60Request.getSourceContains().isEmpty()) {
					httpClient.queryParameter("SourceContains", iti60Request.getSourceContains());
				}
				if (iti60Request.getPurposeContains() != null && !iti60Request.getPurposeContains().isEmpty()) {
					httpClient.queryParameter("PurposeContains", iti60Request.getPurposeContains());
				}
				if (iti60Request.getDefinitionContains() != null && !iti60Request.getDefinitionContains().isEmpty()) {
					httpClient.queryParameter("DefinitionContains", iti60Request.getDefinitionContains());
				}
				if (iti60Request.getGroupContains() != null && !iti60Request.getGroupContains().isEmpty()) {
					httpClient.queryParameter("GroupContains", iti60Request.getGroupContains());
				}
				if (iti60Request.getGroupOID() != null && !iti60Request.getGroupOID().isEmpty()) {
					httpClient.queryParameter("GroupOID", iti60Request.getGroupOID());
				}
				if (iti60Request.getEffectiveDateBefore() != null) {
					httpClient.queryParameter("EffectiveDateBefore", iti60Request.getEffectiveDateBefore());
				}
				if (iti60Request.getEffectiveDateAfter() != null) {
					httpClient.queryParameter("EffectiveDateAfter", iti60Request.getEffectiveDateAfter());
				}
				if (iti60Request.getExpirationDateBefore() != null) {
					httpClient.queryParameter("ExpirationDateBefore", iti60Request.getExpirationDateBefore());
				}
				if (iti60Request.getExpirationDateAfter() != null) {
					httpClient.queryParameter("ExpirationDateAfter", iti60Request.getExpirationDateAfter());
				}
				if (iti60Request.getCreationDateBefore() != null) {
					httpClient.queryParameter("CreationDateBefore", iti60Request.getCreationDateBefore());
				}
				if (iti60Request.getCreationDateAfter() != null) {
					httpClient.queryParameter("CreationDateAfter", iti60Request.getCreationDateAfter());
				}
				if (iti60Request.getRevisionDateBefore() != null) {
					httpClient.queryParameter("RevisionDateBefore", iti60Request.getRevisionDateBefore());
				}
				if (iti60Request.getRevisionDateAfter() != null) {
					httpClient.queryParameter("RevisionDateAfter", iti60Request.getRevisionDateAfter());
				}
			} catch (RuntimeException e) {
				responseStatus = null;
				return null;
			}
		}

		ClientResponse<?> response = null;
		try {
			response = httpClient.get();
			if (response.getStatus() == 200) {
				responseStatus = null;
				return response.getEntity(String.class);
			} else if (response.getStatus() == 404) {
				MultivaluedMap<String, String> headers = response.getHeaders();
				String warnHeader = headers.getFirst("Warning");
				if (warnHeader.contains(RepositoryCore.NAV)) {
					responseStatus = RepositoryCore.NAV;
				} else if (warnHeader.contains(RepositoryCore.INV)) {
					responseStatus = RepositoryCore.INV;
				} else if (warnHeader.contains(RepositoryCore.VERUNK)) {
					responseStatus = RepositoryCore.VERUNK;
				}
				return warnHeader;
			} else {
				responseStatus = "Error " + Integer.toString(response.getStatus());
				return null;
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return null;

		}

	}
}
