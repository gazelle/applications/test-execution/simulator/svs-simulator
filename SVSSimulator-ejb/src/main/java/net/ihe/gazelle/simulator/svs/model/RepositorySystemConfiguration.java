package net.ihe.gazelle.simulator.svs.model;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.sut.model.*;
import org.hibernate.exception.ConstraintViolationException;
import org.jboss.seam.Component;
import org.jboss.seam.faces.FacesMessages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PersistenceException;
import java.util.Date;

/**
 * Created by xfs on 04/03/16.
 */
@Entity
public class RepositorySystemConfiguration extends SystemConfiguration {

    private static final long serialVersionUID = 5444985974251904657L;
    private static Logger log = LoggerFactory.getLogger(SystemConfiguration.class);

    @Column(name = "last_changed")
    private Date lastChanged;

    @Column(name = "binding_type")
    private String bindingType;

    public enum BindingType {
        HTTP("HTTP"),
        SOAP("SOAP");

        private String value;

        private BindingType(String value) {
            this.value = value;
        }

        public String getValue() {
            return this.value;
        }
    }

    /**
     * Creates or updates a system configuration
     *
     * @param inConfiguration : the configuration to create or merge
     */
    public RepositorySystemConfiguration(SystemConfiguration inConfiguration) {
        super(inConfiguration);
        this.setLastChanged(new Date());
    }

    public RepositorySystemConfiguration() {
        setIsAvailable(true);
        setIsPublic(true);
        setLastChanged(new Date());
    }

    public Date getLastChanged() {
        return lastChanged;
    }

    public void setLastChanged(Date lastChanged) {
        this.lastChanged = lastChanged;
    }

    public String getBindingType() {
        return bindingType;
    }

    public void setBindingType(String bindingType) {
        this.bindingType = bindingType;
    }

}
