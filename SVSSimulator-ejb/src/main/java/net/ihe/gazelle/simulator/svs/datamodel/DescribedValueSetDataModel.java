package net.ihe.gazelle.simulator.svs.datamodel;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.restrictions.HQLRestrictionLikeMatchMode;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.simulator.svs.action.ValueSetManager;
import net.ihe.gazelle.svs.DescribedValueSet;
import net.ihe.gazelle.svs.GroupType;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DescribedValueSetDataModel extends FilterDataModel<DescribedValueSet> {

	private DescribedValueSet vsExtract;
	private ValueSetManager.ExtendedFilter requestExtract;

	public DescribedValueSetDataModel(Filter<DescribedValueSet> filter, DescribedValueSet valueSet,
			ValueSetManager.ExtendedFilter request) {
		super(filter);
		this.vsExtract = valueSet;
		this.requestExtract = request;

		if (this.vsExtract == null) {
			this.vsExtract = new DescribedValueSet();
		}
	}

	@Override
	public void appendFiltersFields(HQLQueryBuilder<DescribedValueSet> queryBuilder) {

		if (this.vsExtract.getId() != null && !this.vsExtract.getId().isEmpty()) {
			queryBuilder.addRestriction(HQLRestrictions.like("id", vsExtract.getId(),
					HQLRestrictionLikeMatchMode.ANYWHERE));
		}
		if (vsExtract.getDisplayName() != null && !vsExtract.getDisplayName().isEmpty()) {
			queryBuilder.addRestriction(HQLRestrictions.like("displayName", vsExtract.getDisplayName(),
					HQLRestrictionLikeMatchMode.ANYWHERE));
		}
		if (vsExtract.getVersion() != null && !vsExtract.getVersion().isEmpty()) {
			queryBuilder.addRestriction(HQLRestrictions.like("version", vsExtract.getVersion(),
					HQLRestrictionLikeMatchMode.ANYWHERE));
		}
		if (vsExtract.getPurpose() != null && !vsExtract.getPurpose().isEmpty()) {
			queryBuilder.addRestriction(HQLRestrictions.like("purpose", vsExtract.getPurpose(),
					HQLRestrictionLikeMatchMode.ANYWHERE));
		}
		if (requestExtract.getEffectiveDateBefore() != null) {
			queryBuilder
					.addRestriction(HQLRestrictions.le("effectiveDateUtil", requestExtract.getEffectiveDateBefore()));
		}
		if (requestExtract.getEffectiveDateAfter() != null) {
			queryBuilder
					.addRestriction(HQLRestrictions.ge("effectiveDateUtil", requestExtract.getEffectiveDateAfter()));
		}
		if (requestExtract.getExpirationDateBefore() != null) {
			queryBuilder.addRestriction(HQLRestrictions.le("expirationDateUtil",
					requestExtract.getExpirationDateBefore()));
		}
		if (requestExtract.getExpirationDateAfter() != null) {
			queryBuilder.addRestriction(HQLRestrictions.ge("expirationDateUtil",
					requestExtract.getExpirationDateAfter()));
		}
		if (requestExtract.getRevisionDateBefore() != null) {
			queryBuilder.addRestriction(HQLRestrictions.le("revisionDateUtil", requestExtract.getRevisionDateBefore()));
		}
		if (requestExtract.getRevisionDateAfter() != null) {
			queryBuilder.addRestriction(HQLRestrictions.ge("revisionDateUtil", requestExtract.getRevisionDateAfter()));
		}
		if (requestExtract.getCreationDateBefore() != null) {
			queryBuilder.addRestriction(HQLRestrictions.le("creationDateUtil", requestExtract.getCreationDateBefore()));
		}
		if (requestExtract.getCreationDateAfter() != null) {
			queryBuilder.addRestriction(HQLRestrictions.ge("creationDateUtil", requestExtract.getCreationDateAfter()));
		}

		// Group Part

		if (requestExtract.getGroupOID() != null && !requestExtract.getGroupOID().isEmpty()) {
			queryBuilder.addRestriction(HQLRestrictions.eq("group.id", requestExtract.getGroupOID()));
		}

		if (requestExtract.getNotGroupOID() != null && !requestExtract.getNotGroupOID().isEmpty()) {
			HQLQueryBuilder<GroupType> subQuyer = new HQLQueryBuilder<GroupType>(GroupType.class);
			subQuyer.addEq("iD", requestExtract.getNotGroupOID());
			GroupType groupType = subQuyer.getUniqueResult();
			List<DescribedValueSet> valueSets = groupType.getValueSets();
			Set<Integer> ids = new HashSet<Integer>();
			for (DescribedValueSet describedValueSet : valueSets) {
				ids.add(describedValueSet.getIdDB());
			}
			queryBuilder.addRestriction(HQLRestrictions.nin("this.idDB", ids));
		}

	}

	public DescribedValueSet getVsExtract() {
		if (vsExtract == null) {
			vsExtract = new DescribedValueSet();
		}
		return vsExtract;
	}

	public void setVsExtract(DescribedValueSet vsExtract) {
		this.vsExtract = vsExtract;
	}

@Override
        protected Object getId(DescribedValueSet t) {
        // TODO Auto-generated method stub
        return t.getId();
        }
}
