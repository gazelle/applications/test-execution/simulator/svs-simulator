<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	version="1.0">
	<xsl:output encoding="utf-8" indent="yes" method="html"
		omit-xml-declaration="yes" />

	<xsl:template match="RetrieveValueSetRequest">
		<div>
			<h3>Request parameters:</h3>
			<table>
				<tr>
					<td>
						<strong>id </strong>
					</td>
					<td>
						<xsl:value-of select="ValueSet/@id" />
					</td>
				</tr>
				<xsl:if test="string-length(ValueSet/@version) &gt; 0">
				<tr>
					<td>
						<strong>version </strong>
					</td>
					<td>
						<xsl:value-of select="ValueSet/@version" />
					</td>
				</tr>
				</xsl:if>
				<xsl:if test="string-length(ValueSet/@xml:lang) &gt; 0">
				<tr>
					<td>
						<strong>lang </strong>
					</td>
					<td>
						<xsl:value-of select="ValueSet/@xml:lang" />
					</td>
				</tr>
				</xsl:if>
			</table>
		</div>
	</xsl:template>
	<xsl:template match="RetrieveMultipleValueSetsRequest">
		<div>
			<h3>Request parameters:</h3>
			<table>
				<xsl:if test="id">
					<tr>
						<td>
							<strong>id </strong>
						</td>
						<td>
							<xsl:value-of select="id" />
						</td>
					</tr>
				</xsl:if>
				<xsl:if test="DisplayNameContains">
					<tr>
						<td>
							<strong>DisplayNameContains </strong>
						</td>
						<td>
							<xsl:value-of select="DisplayNameContains" />
						</td>
					</tr>
				</xsl:if>
				<xsl:if test="SourceContains">
					<tr>
						<td>
							<strong>SourceContains </strong>
						</td>
						<td>
							<xsl:value-of select="SourceContains" />
						</td>
					</tr>
				</xsl:if>
				<xsl:if test="PurposeContains">
					<tr>
						<td>
							<strong>PurposeContains </strong>
						</td>
						<td>
							<xsl:value-of select="PurposeContains" />
						</td>
					</tr>
				</xsl:if>
				<xsl:if test="DefinitionContains">
					<tr>
						<td>
							<strong>DefinitionContains </strong>
						</td>
						<td>
							<xsl:value-of select="DefinitionContains" />
						</td>
					</tr>
				</xsl:if>
				<xsl:if test="GroupContains">
					<tr>
						<td>
							<strong>GroupContains </strong>
						</td>
						<td>
							<xsl:value-of select="GroupContains" />
						</td>
					</tr>
				</xsl:if>
				<xsl:if test="GroupOID">
					<tr>
						<td>
							<strong>GroupOID </strong>
						</td>
						<td>
							<xsl:value-of select="GroupOID" />
						</td>
					</tr>
				</xsl:if>
				<xsl:if test="EffectiveDateBefore">
					<tr>
						<td>
							<strong>EffectiveDateBefore </strong>
						</td>
						<td>
							<xsl:value-of select="EffectiveDateBefore" />
						</td>
					</tr>
				</xsl:if>
				<xsl:if test="EffectiveDateAfter">
					<tr>
						<td>
							<strong>EffectiveDateAfter </strong>
						</td>
						<td>
							<xsl:value-of select="EffectiveDateAfter" />
						</td>
					</tr>
				</xsl:if>
				<xsl:if test="ExpirationDateBefore">
					<tr>
						<td>
							<strong>ExpirationDateBefore </strong>
						</td>
						<td>
							<xsl:value-of select="ExpirationDateBefore" />
						</td>
					</tr>
				</xsl:if>
				<xsl:if test="ExpirationDateAfter">
					<tr>
						<td>
							<strong>ExpirationDateAfter </strong>
						</td>
						<td>
							<xsl:value-of select="ExpirationDateAfter" />
						</td>
					</tr>
				</xsl:if>
				<xsl:if test="CreationDateBefore">
					<tr>
						<td>
							<strong>CreationDateBefore </strong>
						</td>
						<td>
							<xsl:value-of select="CreationDateBefore" />
						</td>
					</tr>
				</xsl:if>
				<xsl:if test="CreationDateAfter">
					<tr>
						<td>
							<strong>CreationDateAfter </strong>
						</td>
						<td>
							<xsl:value-of select="CreationDateAfter" />
						</td>
					</tr>
				</xsl:if>
				<xsl:if test="RevisionDateBefore">
					<tr>
						<td>
							<strong>RevisionDateBefore </strong>
						</td>
						<td>
							<xsl:value-of select="RevisionDateBefore" />
						</td>
					</tr>
				</xsl:if>
				<xsl:if test="RevisionDateAfter">
					<tr>
						<td>
							<strong>RevisionDateAfter </strong>
						</td>
						<td>
							<xsl:value-of select="RevisionDateAfter" />
						</td>
					</tr>
				</xsl:if>
			</table>
		</div>
	</xsl:template>
</xsl:stylesheet>
