<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				version="2.0">

	<xsl:output indent="yes" method="xml" />

	<xsl:param name="searchedCode" />

	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()">
				<xsl:with-param name="searchedCode" />
			</xsl:apply-templates>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="node()[name() = 'Concept']">
		<xsl:if test="@code = $searchedCode">
			<xsl:copy>
				<xsl:apply-templates select="@*|node()" />
			</xsl:copy>
		</xsl:if>
	</xsl:template>

	<xsl:template match="comment()" />
</xsl:stylesheet>