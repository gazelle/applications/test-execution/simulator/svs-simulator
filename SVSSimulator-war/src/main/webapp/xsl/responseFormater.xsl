<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	version="1.0">
	<xsl:output encoding="utf-8" indent="yes" method="xml"
		omit-xml-declaration="yes" />
	<xsl:template match="/">

		<xsl:choose>
			<xsl:when test="RetrieveValueSetResponse">
				<div>
					<table>
						<tr>
							<td>
								<strong>id </strong>
							</td>
							<td>
								<xsl:value-of select="RetrieveValueSetResponse/ValueSet/@id" />
							</td>
						</tr>
						<tr>
							<td>
								<strong>DisplayName </strong>
							</td>
							<td>
								<xsl:value-of select="RetrieveValueSetResponse/ValueSet/@displayName" />
							</td>
						</tr>
						<tr>
							<td>
								<strong>Version </strong>
							</td>
							<td>
								<xsl:value-of select="RetrieveValueSetResponse/ValueSet/@version" />
							</td>
						</tr>
					</table>
					<xsl:for-each select="RetrieveValueSetResponse/ValueSet/ConceptList">
						<p>
							<strong>Lang: </strong>
							<xsl:value-of select="@xml:lang" />
						</p>
						<table class="table table-striped" width="100%" border="1" cellspacing="0" cellpadding="0">
							<tr>
								<th scope="col">code</th>
								<th scope="col">displayName</th>
								<th scope="col">codeSystem</th>
								<th scope="col">codeSystemName</th>
								<th scope="col">codeSystemVersion</th>
							</tr>
							<xsl:for-each select="Concept">
								<tr>
									<td>
										<xsl:value-of select="@code" />
									</td>
									<td>
										<xsl:value-of select="@displayName" />
									</td>
									<td>
										<xsl:value-of select="@codeSystem" />
									</td>
									<td>
										<xsl:value-of select="@codeSystemName" />
									</td>
									<td>
										<xsl:value-of select="@codeSystemVersion" />
									</td>
								</tr>
							</xsl:for-each>
						</table>
					</xsl:for-each>
				</div>
			</xsl:when>
			<xsl:otherwise>
				<div>
					<xsl:for-each select="RetrieveMultipleValueSetsResponse/DescribedValueSet">
						<table>
							<tr>
								<td>
									<strong>id </strong>
								</td>
								<td>
									<xsl:value-of select="@id" />
								</td>
							</tr>
							<tr>
								<td>
									<strong>DisplayName </strong>
								</td>
								<td>
									<xsl:value-of select="@displayName" />
								</td>
							</tr>
							<tr>
								<td>
									<strong>Version </strong>
								</td>
								<td>
									<xsl:value-of select="@version" />
								</td>
							</tr>
						</table>
						<xsl:for-each select="ConceptList">
							<p>
								<strong>Lang: </strong>
								<xsl:value-of select="@xml:lang" />
							</p>
							<table class="table table-striped" width="100%" border="1" cellspacing="0" cellpadding="0">
								<tr>
									<th scope="col">code</th>
									<th scope="col">displayName</th>
									<th scope="col">codeSystem</th>
									<th scope="col">codeSystemName</th>
									<th scope="col">codeSystemVersion</th>
								</tr>
								<xsl:for-each select="Concept">
									<tr>
										<td>
											<xsl:value-of select="@code" />
										</td>
										<td>
											<xsl:value-of select="@displayName" />
										</td>
										<td>
											<xsl:value-of select="@codeSystem" />
										</td>
										<td>
											<xsl:value-of select="@codeSystemName" />
										</td>
										<td>
											<xsl:value-of select="@codeSystemVersion" />
										</td>
									</tr>
								</xsl:for-each>
							</table>
						</xsl:for-each>
						<hr />
						<br />
					</xsl:for-each>
				</div>
			</xsl:otherwise>
		</xsl:choose>

	</xsl:template>
</xsl:stylesheet>
