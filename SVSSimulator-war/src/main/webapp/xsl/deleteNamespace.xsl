<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	version="1.0">
	<xsl:output encoding="utf-8" indent="yes" method="xml"
		omit-xml-declaration="yes" />

	<xsl:strip-space elements="*" />

	<xsl:template match="@*|node()[not(self::*)]">
		<xsl:copy />
	</xsl:template>

	<xsl:template match="*">
		<xsl:element name="{local-name()}">
			<xsl:apply-templates select="node()|@*" />
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>