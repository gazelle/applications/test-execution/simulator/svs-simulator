var nextDeployDate = null;
var countdownTimer = null;
var warnTime = 59 * 60 * 1000;
var localServerTimeDiff;

function padTwoChars(number) {
	var str = '' + number;
	while (str.length < 2) {
		str = '0' + str;
	}
	return str;
}

function updateCountdown() {
	if (nextDeployDate != null) {
		var nowDate = new Date();
		var nowTime = nowDate.getTime();
		var nowTimeServer = nowTime - localServerTimeDiff;

		var diffTime = (nextDeployDate.getTime() - nowTimeServer) / 1000;
		if (diffTime > 0) {
			var minutesLeft = padTwoChars(Math.floor(diffTime / 60));
			var secondsLeft = padTwoChars(Math.floor(diffTime
					- (60 * minutesLeft)));
			var timeLeft = minutesLeft + ":" + secondsLeft;
			document.getElementById('deployCountdown').innerHTML = timeLeft;
			setTimeout("updateCountdown();", 1000);
		} else {
			document.getElementById('deployScheduled').className = 'schedule-invisible';
			document.getElementById('deployExecuted').className = 'schedule-visible gzl-alert gzl-alert-red';
		}
	}
}

function startCountDown(nextDeployTime, serverDateTime) {
	var nowDate = new Date();
	var nowTime = nowDate.getTime();

	localServerTimeDiff = nowTime - serverDateTime;

	var diffTime = nextDeployTime - serverDateTime;

	if (diffTime > 0 && diffTime < warnTime) {

		if (nextDeployDate == null
				|| nextDeployDate.getTime() != nextDeployTime) {
			nextDeployDate = new Date();
			nextDeployDate.setTime(nextDeployTime);
		}

		updateCountdown();
		setTimeout("updateCountdown();", 1000);
		document.getElementById('deployScheduled').className = 'schedule-visible gzl-alert gzl-alert-red';
	} else {
		stopCountDown();
	}

}

function stopCountDown() {
	nextDeployDate = null;
	document.getElementById('deployScheduled').className = 'schedule-invisible';
}
