var notificationMap = {};

function hideMessage(message) {
	if (notificationMap[message]) {
		notificationMap[message].cancel();
	}
}

function checkNewMessages(messagesJSON64) {
	var messagesJSON = window.atob(messagesJSON64);
	var messages = window.eval(messagesJSON);
	for ( var i = 0; i < messages.length; i++) {
		var messageArray = messages[i];

		if (window.webkitNotifications) {
			var notification = window.webkitNotifications.createNotification(
					messageArray[4], messageArray[0], messageArray[1]);
			notification.onclick = function(x) {
				window.open(messageArray[2]);
				this.cancel();
			};
			notification.show();
			notificationMap[messageArray[3]] = notification;
			setTimeout("hideMessage('" + messageArray[3] + "')", 10000);
		}
	}
}

function openMessages() {
	if (window.webkitNotifications) {
		if (window.webkitNotifications.checkPermission() == 1) {
			window.webkitNotifications.requestPermission();
		}
	}
	RichFaces.showModalPanel('lastMessagesModalPanel');
	document.getElementById('mbf:messageCounterButton').value = '0';
}
