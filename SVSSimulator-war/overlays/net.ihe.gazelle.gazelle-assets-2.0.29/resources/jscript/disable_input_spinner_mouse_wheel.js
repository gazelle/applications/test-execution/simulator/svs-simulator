function disable_input_spinner_mouse_wheel(component_id) {
    $(document).ready(function () {
        RichFaces.$(component_id).input.unbind('mousewheel');
    });
}