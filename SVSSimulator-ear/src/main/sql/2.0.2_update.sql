INSERT INTO public.app_configuration (id, value, variable) VALUES (nextval('app_configuration_id_seq'), 'Anne-Gaelle BERGE', 'application_admin_name');
INSERT INTO public.app_configuration (id, value, variable) VALUES (nextval('app_configuration_id_seq'), 'abe@kereval.com', 'application_admin_email');
INSERT INTO public.app_configuration (id, value, variable) VALUES (nextval('app_configuration_id_seq'), 'Application Administrator', 'application_admin_title');
