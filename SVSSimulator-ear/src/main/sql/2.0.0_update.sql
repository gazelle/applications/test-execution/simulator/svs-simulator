INSERT INTO system_configuration(id, is_available, is_public, name, owner, system_name, url, binding_type, dtype, last_changed)
    SELECT
      nextval('system_configuration_id_seq'),
      active,
      shared,
      name,
      username,
      name,
      endpoint,
      binding_type,
      'RepositorySystemConfiguration',
      last_changed
    FROM svs_sut;

DROP TABLE svs_sut;

INSERT INTO affinity_domain(id, keyword, label_to_display, profile) VALUES (1,'IHE','IHE','All');
INSERT INTO usage_metadata(id, action, affinity_id, transaction_id, keyword) VALUES (1,'ITI-48',1,1,'ITI-48');
INSERT INTO usage_metadata(id, action, affinity_id, transaction_id, keyword) VALUES (2,'ITI-60',1,2,'ITI-60');

UPDATE svs_conceptlist
SET lang = 'en-US'
WHERE lang IS NULL;

ALTER TABLE system_configuration
DROP COLUMN actor_id;